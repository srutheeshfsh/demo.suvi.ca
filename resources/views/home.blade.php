@extends('layouts.app')

@section('content')
<main>
    <!-- slider area start -->
    <section class="slider-area gray-bg">
         @php
                $section = App\Section::where('name', 'New Updates')->first();
            @endphp
            @if ($section)
                <div class="new_updates alert alert-info alert-dismisable fade show mb-0" role="alert">
                    {!! $section->content !!}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
        <video autoplay="autoplay" loop="loop" muted="" preload="true" playsinline="" id="myVideo"
            style="width:100%; object-fit: cover; object-position: 50% 98%; height:85vh;">
            <source src="/SUVI Home banner Video 4.mp4" type="video/mp4">
        </video>
        <div class="container" style="position: absolute; top:30%; padding-left:8%">
            <div class="row">
                <div class="col-xl-12 col-lg-12 ">
                    <div class="silder-text">
                        <div class="slider-caption">
                            <h1 data-animation="fadeInUp" data-delay=".2s">Specialty Use Vehicle
                                Incentive Program</h1>
                            <!-- <p data-animation="fadeInUp" data-delay=".4s">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p> -->
                        </div>
                    </div>
                    <div class="slider-btn">
                        <a data-animation="fadeInLeft" data-delay=".6s" class="thm-btn"
                            href="{{ route('apply') }}">Apply</a>
                        <a data-animation="fadeInRight" data-delay=".8s" class="thm-btn border-btn"
                            href="{{ route('special-utility-vehicle-incentive-program') }}">Learn More</a>
                    </div>
                </div>
            </div>
        </div>
        {{-- <div class="slider-active">
            <div class="single-slider slider-height d-flex align-items-center" data-background="/img/bg/01.jpg">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 ">
                            <div class="slider-shape">
                                <!--<img src="/img/shape/slider1.png" alt="">-->
                            </div>
                            <div class="slider-content text-left">
                                <div class="silder-text">
                                    <div class="slider-caption">
                                        <h1 data-animation="fadeInUp" data-delay=".2s">Specialty Use Vehicle
                                            Incentives</h1>
                                        <!-- <p data-animation="fadeInUp" data-delay=".4s">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p> -->
                                    </div>
                                </div>
                                <div class="slider-btn">
                                    <a data-animation="fadeInLeft" data-delay=".6s" class="thm-btn"
                                        href="{{ route('apply') }}">Apply</a>
                                    <a data-animation="fadeInRight" data-delay=".8s" class="thm-btn border-btn"
                                        href="{{ route('special-utility-vehicle-incentive-program') }}">Learn More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="single-slider slider-height d-flex align-items-center" data-background="/img/bg/05.jpg">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 ">
                            <div class="slider-shape">
                                <!--<img src="/img/shape/slider2.png" alt="">-->
                            </div>
                            <div class="slider-content text-left">
                                <div class="silder-text">
                                    <div class="slider-caption">
                                        <h1 data-animation="fadeInUp" data-delay=".2s">Specialty Use Vehicle
                                            Incentives</h1>
                                        <!-- <p data-animation="fadeInUp" data-delay=".4s">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p> -->
                                    </div>
                                </div>
                                <div class="slider-btn">
                                    <a data-animation="fadeInLeft" data-delay=".6s" class="thm-btn"
                                        href="{{ route('apply') }}">Apply</a>
                                    <a data-animation="fadeInRight" data-delay=".8s" class="thm-btn border-btn"
                                        href="{{ route('special-utility-vehicle-incentive-program') }}">Learn More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="single-slider slider-height d-flex align-items-center" data-background="/img/bg/03.jpg">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 ">
                            <div class="slider-shape">
                                <!--<img src="/img/shape/slider-3.png" alt="">-->
                            </div>
                            <div class="slider-content text-left">
                                <div class="silder-text">
                                    <div class="slider-caption">
                                        <h1 data-animation="fadeInUp" data-delay=".2s">Specialty Use Vehicle
                                            Incentives</h1>
                                        <!-- <p data-animation="fadeInUp" data-delay=".4s">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p> -->
                                    </div>
                                </div>
                                <div class="slider-btn">
                                    <a data-animation="fadeInLeft" data-delay=".6s" class="thm-btn"
                                        href="{{ route('apply') }}">Apply</a>
                                    <a data-animation="fadeInRight" data-delay=".8s" class="thm-btn border-btn"
                                        href="{{ route('special-utility-vehicle-incentive-program') }}">Learn More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="single-slider slider-height d-flex align-items-center" data-background="/img/bg/02.jpg">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 ">
                            <div class="slider-shape">
                                <!--<img src="/img/shape/slider4.jpg" alt="">-->
                            </div>
                            <div class="slider-content text-left">
                                <div class="silder-text">
                                    <div class="slider-caption">
                                        <h1 data-animation="fadeInUp" data-delay=".2s">Specialty Use Vehicle
                                            Incentives</h1>
                                        <!-- <p data-animation="fadeInUp" data-delay=".4s">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p> -->
                                    </div>
                                </div>
                                <div class="slider-btn">
                                    <a data-animation="fadeInLeft" data-delay=".6s" class="thm-btn"
                                        href="{{ route('apply') }}">Apply</a>
                                    <a data-animation="fadeInRight" data-delay=".8s" class="thm-btn border-btn"
                                        href="{{ route('special-utility-vehicle-incentive-program') }}">Learn More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="single-slider slider-height d-flex align-items-center" data-background="/img/bg/04.jpg">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 ">
                            <div class="slider-shape">
                                <!--<img src="/img/shape/slider4.png" alt="">-->
                            </div>
                            <div class="slider-content text-left">
                                <div class="silder-text">
                                    <div class="slider-caption">
                                        <h1 data-animation="fadeInUp" data-delay=".2s">Specialty Use Vehicle
                                            Incentives</h1>
                                        <!-- <p data-animation="fadeInUp" data-delay=".4s">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p> -->
                                    </div>
                                </div>
                                <div class="slider-btn">
                                    <a data-animation="fadeInLeft" data-delay=".6s" class="thm-btn"
                                        href="{{ route('apply') }}">Apply</a>
                                    <a data-animation="fadeInRight" data-delay=".8s" class="thm-btn border-btn"
                                        href="{{ route('special-utility-vehicle-incentive-program') }}">Learn More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="single-slider slider-height d-flex align-items-center" data-background="/img/bg/06.jpg">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 ">
                            <div class="slider-shape">
                                <!--<img src="/img/shape/slider4.png" alt="">-->
                            </div>
                            <div class="slider-content text-left">
                                <div class="silder-text">
                                    <div class="slider-caption">
                                        <h1 data-animation="fadeInUp" data-delay=".2s">Specialty Use Vehicle
                                            Incentives</h1>
                                        <!-- <p data-animation="fadeInUp" data-delay=".4s">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p> -->
                                    </div>
                                </div>
                                <div class="slider-btn">
                                    <a data-animation="fadeInLeft" data-delay=".6s" class="thm-btn"
                                        href="{{ route('apply') }}">Apply</a>
                                    <a data-animation="fadeInRight" data-delay=".8s" class="thm-btn border-btn"
                                        href="{{ route('special-utility-vehicle-incentive-program') }}">Learn More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="single-slider slider-height d-flex align-items-center" data-background="/img/bg/07.jpg">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 ">
                            <div class="slider-shape">
                                <!--<img src="/img/shape/slider4.png" alt="">-->
                            </div>
                            <div class="slider-content text-left">
                                <div class="silder-text">
                                    <div class="slider-caption">
                                        <h1 data-animation="fadeInUp" data-delay=".2s">Specialty Use Vehicle
                                            Incentives</h1>
                                        <!-- <p data-animation="fadeInUp" data-delay=".4s">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p> -->
                                    </div>
                                </div>
                                <div class="slider-btn">
                                    <a data-animation="fadeInLeft" data-delay=".6s" class="thm-btn"
                                        href="{{ route('apply') }}">Apply</a>
                                    <a data-animation="fadeInRight" data-delay=".8s" class="thm-btn border-btn"
                                        href="{{ route('special-utility-vehicle-incentive-program') }}">Learn More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}
    </section>
    <!-- slider area end -->

    {{-- <!-- about start -->
    <section class="about-area fix pt-120 pb-90">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-12 order-xl-1 order-md-2 order-1">
                    <div class="about-left">
                        <div class="section-title" data-aos="fade-up" data-aos-delay="100">
                            <div class="bg-title-2">
                                <!-- <h1>About</h1> -->
                            </div>
                            <!-- <h5>About us</h5> -->
                            <h3>The SUVI Program provides <span class="text-custom">rebates</span>
                                on zero-emission motorcycles,
                                cargo e-bikes, utility trucks, buses &
                                more.</h3>
                        </div>
                        <div class="about-text-wrapper">
                            <div class="about-text about-text-2 mb-25" data-aos="fade-up" data-aos-delay="100">
                                <p>The CleanBC Go Electric Specialty Use Vehicle
                                    Incentive (SUVI Program) is a sub-program of the
                                    CleanBC Go Electric Program.
                                </p>
                            </div>
                            <div class="about-text about-text-2 mb-25" data-aos="fade-up" data-aos-delay="200">
                                <p>It is designed to support the adoption of ZEVs in a
                                    variety of vehicles including motorcycles, low-
                                    speed vehicles, electric cargo bicycles (cargo e-
                                    bikes), utility vehicles, and a variety of medium-
                                    and heavy-duty vehicles, referred to as specialty-
                                    use vehicles.</p>
                            </div>
                        </div>
                        <div class="about-btn" data-aos="fade-up" data-aos-delay="400">
                            <a href="/storage/{{ json_decode(setting('site.program_guide'))[0]->download_link }}"
    download="{{ json_decode(setting('site.program_guide'))[0]->original_name }}"
    class="thm-btn thm-btn-2">Learn More</a>
    </div>
    </div>
    </div>
    <div class="col-xl-6 col-lg-12 order-xl-2 order-md-1 order-2">
        <div class="about-right-wrapper">
            <div class="about-right">
                <div class="about-shape-bg">
                    <img src="/img/shape/about-shape-bg.png" alt="">
                </div>
                <div class="about-thumb-big" data-aos="fade-up" data-aos-delay="100">
                    <img src="/img/about/about-bg.png" alt="">
                </div>
                <div class="about-thumb-sml" data-aos="fade-left" data-aos-delay="300">
                    <img src="/img/about/about-small-01.jpg" alt="">
                </div>
            </div>
            <div class="about-right-text" data-aos="fade-right" data-aos-delay="300">
                <img src="/img/about/about-smale-02.png" alt="">
            </div>
        </div>
    </div>
    </div>
    </div>
    </section>
    <!-- about end --> --}}

    @include('components.rebate-sliders')

    @php
        $section = App\Section::where('name', 'Home 4 boxes')->first()
    @endphp
    @if ($section)
    {!! $section->content !!}
    @endif
</main>
@endsection
