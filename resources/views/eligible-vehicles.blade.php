@extends('layouts.app')
@section('head')
  <title>{!!$category->meta_title?$category->meta_title:Voyager::setting('site.title')!!}</title>
  <meta name="description" content="{!!$category->meta_description?$category->meta_description:''!!}">
  <meta name="keywords" content="{!!$category->meta_keywords?$category->meta_keywords:''!!}"> 
@endsection
@section('styles')
{{-- <link rel="stylesheet" type="text/css" href="/vendor/DataTables/datatables.min.css" /> --}}
{{-- <link rel="stylesheet" type="text/css"
    href="https://cdn.datatables.net/v/bs4/dt-1.10.24/sp-1.2.2/sl-1.3.3/datatables.min.css" /> --}}

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('content')
<main>
    {{-- <!--page title start -->
    <section class="page-title-area bg-overly slider-area slider-2" data-overlay="5"
        data-background="/img/bg/vehicle_bg.png">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">

                    <div class="page-title text-center" data-aos="fade-up" data-aos-delay="100">
                        <h1>Eligible Vehicles</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- page title end --> --}}

    <div id="anchor"></div>

    <!-- faq start -->
    <section class="faq-area pt-200 pb-100" id="content">
        <div class="col-md-12 col-12 mb-5">
            <form action="{{ route('vehicles') }}" method="GET" class="search-section">
                <input type="hidden" name="category" value="{{ $queries['category'] }}">
                <div class="container search-vehicle-box">
                    <div class="col-md-12">
                        <h1>{{ $queries['category'] }} <small class="subtext {{$category->sub_text?"":"d-none"}}">( {!! $category->sub_text !!} )</small></h1>
                        
                        <div class="row mb-3 mt-3">
                            <div class="col-md-3 col-sm-12">
                                <label for="make">Make</label>
                                <select id="make" name="make[]" class="form-control select2 " multiple>
                                    @foreach ($filters['make'] as $make)
                                    <option value="{{ $make }}">{{ $make }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <label for="year">Year</label>
                                <select id="year" name="year[]" class="form-control select2 " multiple>
                                    @foreach ($filters['year'] as $year)
                                    <option value="{{ $year }}">{{ $year }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                @if($queries['category'] == "On road Medium and Heavy-Duty")
                                <label for="vehicle_type">Class</label>
                                 <select id="vehicle_type" name="vehicle_type[]" class="form-control select2 " multiple>
                                     <option value="Class 2B"> Class 2B</option>
                                     <option value="Class 3"> Class 3</option>
                                     <option value="Class 4"> Class 4</option>
                                     <option value="Class 5"> Class 5</option>
                                     <option value="Class 6"> Class 6</option>
                                     <option value="Class 7"> Class 7</option>
                                     <option value="Class 8"> Class 8</option>
                                    <!--@foreach ($filters['vehicle_type'] as $vehicle_type)-->
                                    <!--<option value="{{ $vehicle_type }}">{{ $vehicle_type }}</option>-->
                                    <!--@endforeach-->
                                </select>
                                @else
                                <label for="fuel_type">Fuel Type</label>
                               <select id="fuel_type" name="fuel_type[]" class="form-control select2 " multiple>
                                    @foreach ($filters['fuel_type'] as $fuel_type)
                                    <option value="{{ $fuel_type }}">{{ $fuel_type }}</option>
                                    @endforeach
                                </select>
                                 @endif
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <label for="make">MSRP</label>
                                <input type="text" id="msrp" name="base_msrp" readonly
                                    style="border:0; color:#fdb913; font-weight:bold;" class="">
                                <div id="slider-range" class="d-flex justify-content-end "></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-12">
                                <label for="Type the keywords">Type the keywords</label>
                                <input type="text" name="match" id="match" class="form-control" placeholder=""
                                    value="{{ isset($queries['match']) ? $queries['match'] : '' }}">
                            </div>
                            <div class="col-md-3 align-self-end pt-2">
                                <button class="btn  thm-btn thm-btn-2 align-bottom btn-block"
                                    type="submit">Search</button>
                            </div>
                            <div class="col-md-3 align-self-end pt-2">
                                <a href="{{ route('vehicles', ['category' => $queries['category']]) }}"
                                    class="btn thm-btn thm-btn-2 btn-block ">Reset</a>
                            </div>
                            <div class="col-md-3 align-self-end pt-2">
                                <input type="submit" value="Export" class="btn thm-btn thm-btn-2 btn-block"
                                    formaction="{{ route('vehicle.export') }}">
                            </div>
                        </div>
                    </div>
            </form>
        </div>
        @if ($vehicles->total() > 0)
        <div class="container">
            <div class="row mb-3 mt-3">
                <div class="col-md-12">
                    <p>Showing <span class="count"></span> of {{ $vehicles->total() }} results</p>
                </div>
            </div>
            <div class="search-results">
                <ul class="list-group" id="results">
                    @include('components.pagination_data')
                </ul>

                <div class="row">
                    <div class="col-md-12">
                        <p>Showing <span class="count"></span> of {{ $vehicles->total() }} results</p>
                    </div>
                </div>
                <div class="col-md-12 d-flex">
                    <a href="javascript:;" id="load" data-page="2" class="thm-btn thm-btn-2 mt-3 mx-auto">Load more</a>
                </div>
                {{-- {!! $vehicles->links() !!} --}}
            </div>
        </div>
        @else
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p>No results.</p>
                </div>
            </div>
        </div>
        @endif
    </section>
    <!-- faq end -->
</main>
@endsection

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function(){
        document.getElementById('anchor').scrollIntoView({ behavior: 'smooth', block: 'center' });

        $('.select2').select2();

        @isset($queries['make'])
        $('#make').val(@json($queries['make'])).trigger('change');
        @endisset
        @isset($queries['year'])
        $('#year').val(@json($queries['year'])).trigger('change');
        @endisset
        @isset($queries['fuel_type'])
        $('#fuel_type').val(@json($queries['fuel_type'])).trigger('change');
        @endisset

        // $(document).on('click', '.pagination a', function(event){
        //     event.preventDefault();
        //     var page = $(this).attr('href').split('page=')[1];
        //     fetch_data(page);
        // });

        $(document).on('click', '#load', function(event){
            event.preventDefault();
            var page = $(this).data('page');
            fetch_data(page);
            if (page >= {{ $vehicles->lastPage() }}) {
                $(this).remove();
            }
            $(this).data('page', page + 1)
        });

        function fetch_data(page)
        {
            $.ajax({
                url:"/eligible-vehicles",
                type: 'POST',
                // contentType : 'json',
                dataType : 'html',
                // processData: false,
                cache: false,
                data: {
                    "page" : page,
                    @foreach($queries as $key => $value)
                    @if(is_array($value))
                        "{{ $key }}" : @json($value, JSON_FORCE_OBJECT),
                    @else
                        "{{ $key }}" : "{{ $value }}",
                    @endif
                    @endforeach
                },
                success:function(data)
                {
                    $('#results').append(data);
                    $('.count').text($('.result').length);
                }
            });
        }

        $( "#slider-range" ).slider({
            range: true,
            min: {{ $min_base_msrp }},
            max: {{ $max_base_msrp }},
            values: [ {{ $range[0] }}, {{ $range[1] }} ],
            slide: function( event, ui ) {
                $( "#msrp" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
            }
        });

        $( "#msrp" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
            " - $" + $( "#slider-range" ).slider( "values", 1 ) );

        $('.count').text($('.result').length);
    });
</script>
@endsection
