This Program targets specialty-use vehicles that are not included in the CleanBC Go Electric passenger vehicle rebates. Only new vehicles are eligible for incentives; previously used or conversions are not eligible.
In the case of cargo e-bikes not currently on the eligible vehicle list that don’t have a standard model, such as a non-electric cargo e-bike converted using an electric kit, rebate support can potentially be offered as long as the cargo bicycle and electrification kit are newly purchased. Please email SUVI@pluginbc.ca.
Additional eligibility information
To receive a vehicle rebate all road vehicles must remain plated, registered, and insured in British Columbia in the applicant’s name for at least 12 months for the low-speed vehicle, cargo e-bikes, motorcycles and utility vehicle categories, and at least 36 months for on-road medium and heavy-duty vehicle and airport and port vehicle categories. If, before the 12 or 36 months is up, the applicant does not abide by all of the above conditions, the applicant will be required to repay the full amount of the rebate awarded.
Vehicles must meet the following criteria to be eligible for an incentive:
Be New
To be eligible, the vehicle must be a new vehicle constructed entirely from new parts that have never been the subject of a retail sale, or previously registered in British Columbia or another jurisdiction. The Original Equipment Manufacturer (OEM) or its authorized licensee must manufacture the vehicle. Registration of Out-of-Province Vehicles are not eligible vehicles. If the vehicle is not new, has been re-leased, is the subject of a lease assumption or has been transferred into British Columbia after previously having been registered out of province, the vehicle is not eligible for an incentive through the program. Aftermarket plug-in hybrid electric vehicle conversions are not eligible for funding.
Be Certified and Meet Definitions
If the vehicle is an electric motorcycle, to be eligible:
The vehicle that is of the subclasses enclosed motorcycle, open motorcycle, limited-speed motorcycle or motor tricycle, and
•	is designed to travel on not more than three wheels in contact with the ground
•	has a minimum wheel rim diameter of 250 mm, and
•	has a minimum wheelbase of 1 016 mm,
but does not include a power-assisted bicycle, a restricted-use motorcycle, a passenger car, a truck, a multi-purpose passenger vehicle, a competition vehicle, a vehicle imported temporarily for special purposes or a three-wheeled vehicle.
And therefore:
The vehicle must be in the “open motorcycle” or “motor tricycle” category as defined by the Motor Vehicle Safety Regulations (CRC, c. 1038) and be entirely powered by electricity.
Or
The vehicle must be in the limited-speed motorcycle category as defined by the Motor Vehicle Safety Regulations and be entirely powered by electricity. The vehicle:
•	has steering handlebars that are completely constrained from rotating in relation to the axle of only one wheel in contact with the ground,
•	has a maximum speed of 70 km/h or less,
•	has a minimum driver’s seat height, when the vehicle is unladen, of 650 mm, and
•	does not have a structure partially or fully enclosing the driver and passenger, other than that part of the vehicle forward of the driver’s torso and the seat backrest
If the vehicle is a Low Speed Vehicle, to be eligible:
The vehicle must meet the neighbourhood zero emission vehicle as defined in Division 1 of the Motor Vehicle Act Regulations, B.C. Reg. 26/58.
If the vehicle is on road medium or heavy duty, to be eligible:
The vehicle must be either covered by a US Environmental Protection Agency (EPA) certificate or be deemed to be covered by an EPA certificate in accordance with the On-Road Vehicle and Engine Emission Regulation under the Canadian Environmental Protection Act, 1999 (CEPA,1999), and must meet all requirements outlined in Transport Canada’s Motor Vehicle Safety Act and its regulations.
In addition, vehicles must have a gross vehicle weight rating above 4,536 kg (Class 3 and higher).
If the vehicle is an Airport/Port vehicle, to be eligible:
The vehicle must meet any Federal safety or operation certification required for intended operation at the expected location.
Be Battery Electric, Plugin Hybrid or Hydrogen Fuel Cell
To be eligible, the vehicle must be either fully battery electric, plug-in hybrid, or a hydrogen fuel cell. A conventional hybrid vehicle is not eligible for this Program.
Time in British Columbia
To receive a vehicle rebate all road vehicles must remain plated, registered, and insured in British Columbia in the applicant’s name for at least 12 months for the low-speed vehicle, airport and port equipment and utility vehicle categories, and at least 36 months for on-road medium and heavy-duty vehicle and airport and port vehicle categories. If, before the 12 or 36 months is up, the applicant does not abide by all of the above conditions, the applicant will be required to repay the full amount of the rebate awarded.
Purchased or Leased in Canada
To be eligible for this Program the vehicle must be purchased or leased within Canada. Vehicles purchased outside Canada will not be eligible.
