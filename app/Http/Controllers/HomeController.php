<?php

namespace App\Http\Controllers;

use App\Event;
use App\Faq;
use App\Helpers\Image;
use App\Imports\VehicleImport;
use App\ManufacturerApplication;
use App\Notifications\ManufacturerApplication as NotificationsManufacturerApplication;
use App\Page;
use App\Vehicle;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class HomeController extends Controller
{
    public function index()
    {
        return view('home');
    }

    public function results()
    {
        return view('results');
    }

    public function specialUtilityVehicleIncentiveProgram()
    {
        $page = Page::where('category', 'none')->where('slug', 'specialty-use-vehicle-incentive-program')->firstOrFail();

        return view('specialty-use-vehicle-incentive-program', compact('page'));
    }

    public function programGuideAndEligibilityCriteria()
    {
        $page = Page::where('category', 'none')->where('slug', 'program-guide-and-eligibility-criteria')->firstOrFail();

        return view('program-guide-and-eligibility-criteria', compact('page'));
    }

    public function rebate()
    {
        return view('rebate');
    }

    public function rebateDetail(Request $request)
    {
        $page = Page::where('category', 'rebate')->where('slug', $request['slug'])->firstOrFail();

        return view('rebate-detail', compact('page'));
    }

    public function faq()
    {
        $faqs = Faq::all();

        return view('faq', compact('faqs'));
    }

    public function storiesAndEvents()
    {
        $stories = Page::where('category', 'success-story')->get();
        $events = Event::whereDate('start', '=', today())->get();

        return view('stories-and-events', compact('events', 'stories'));
    }

    public function successStory(Request $request)
    {
        $page = Page::where('category', 'success-story')->where('slug', $request['slug'])->firstOrFail();

        return view('success-story', compact('page'));
    }

    public function getEvents(Request $request)
    {
        if ($request->ajax()) {
            $events = Event::all();

            return $events;
        } else {
            abort(403);
        }
    }

    public function addVehicle(Request $request)
    {
        if ($request->isMethod("GET")) {
            return view('add_vehicle');
        } else if ($request->isMethod("POST")) {
            $validated = $request->validate([
                'manufacturer_name' => 'required|max:255',
                'representitive_name' => 'required|max:255',
                'representitive_position' => 'required|max:255',
                'contact_email' => 'required|email|max:255',
                'contact_phone' => ['required', 'min:7', 'max:255', 'regex:/(^[+]?)(([0-9]|[ ()-.])*)([x]?[ ]?([0-9])*)/'],
                'sign' => 'nullable|max:255',

                'vehicle_category' => 'required|array',
                'vehicle_category.*' => 'required|max:255',
                'vehicle_make' => 'required|array',
                'vehicle_make.*' => 'required|max:255',
                'model_year' => 'required|array',
                'model_year.*' => 'required|integer',
                'vehicle_model' => 'required|array',
                'vehicle_model.*' => 'required|max:255',
                'vehicle_type' => 'required|array',
                'vehicle_type.*' => 'required|max:255',
                'vehicle_fuel_type' => 'required|array',
                'vehicle_fuel_type.*' => 'required|max:255',
                'vehicle_battery_size' => 'required|array',
                'vehicle_battery_size.*' => 'required|max:255',
                'msrp' => 'required|array',
                'msrp.*' => 'required|max:255',
                'image' => 'required|array',
                'image.*' => 'required|image|mimes:jpeg,png,jpg|max:1024',

                recaptchaFieldName() => recaptchaRuleName(),
            ]);

            unset($validated[recaptchaFieldName()]);

            $forms = [];
            $count = 1;
            foreach ($validated['image'] as $index => $image) {
                $imageName = time() . "-$count." . $image->extension();
                $image = Image::storeImage($image, $imageName);
//dd($image);
                $form = ManufacturerApplication::create([
                    'manufacturer_name' => $validated['manufacturer_name'],
                    'representitive_name' => $validated['representitive_name'],
                    'representitive_position' => $validated['representitive_position'],
                    'contact_email' => $validated['contact_email'],
                    'contact_phone' => $validated['contact_phone'],
                    'sign' => $validated['sign'],

                    'vehicle_category' => $validated['vehicle_category'][$index],
                    'vehicle_make' => $validated['vehicle_make'][$index],
                    'model_year' => $validated['model_year'][$index],
                    'vehicle_model' => $validated['vehicle_model'][$index],
                    'vehicle_type' => $validated['vehicle_type'][$index],
                    'vehicle_fuel_type' => $validated['vehicle_fuel_type'][$index],
                    'vehicle_battery_size' => $validated['vehicle_battery_size'][$index],
                    'msrp' => $validated['msrp'][$index],
                    'image' => $image,
                ]);
                array_push($forms, $form);

                $count++;
            }

            if (config('app.env') == "production") {
                // foreach ($forms as $form) {
                //     Notification::route('mail', ['suvi@pluginbc.ca','Daniel.Clancy@gov.bc.ca','Kira.Catallo@gov.bc.ca'])
                //         ->notify(new NotificationsManufacturerApplication([$form], false));
                // }
                // Notification::route('mail', [$validated['contact_email']])
                //     ->notify(new NotificationsManufacturerApplication($forms, true));
            } else {
                foreach ($forms as $form) {
                    Notification::route('mail', ['support@fshdesign.org'])
                        ->notify(new NotificationsManufacturerApplication([$form], false));
                }
                Notification::route('mail', ['srutheesh0@gmail.com'])
                    ->notify(new NotificationsManufacturerApplication($forms, true));
            }

            $message = [
                'status' => 'success',
                'message' => 'Your application has been received.'
            ];

            return redirect(route('vehicle.add'))->with('message', $message);
        }
    }

    public function newSection(Request $request)
    {
        $request->validate([
            'index' => ['required', 'numeric']
        ]);

        return view('components.vehivle-detail-form', ["index" => $request['index']])->render();
    }

    public function apply()
    {
        return view('apply');
    }

    public function categories()
    {
        return view('categories');
    }

    public function vehicles(Request $request)
    {
        if (isset($request['category'])) {
            $vehicles = Vehicle::where('category', $request['category'])->get();
        } else {
            $vehicles = Vehicle::all();
        }

        return view('vehicles', compact('vehicles'));
    }

    public function import(Request $request)
    {
        Excel::import(new VehicleImport, $request->file('file'));

        return back()->with([
            'message'    => "Data is imported successfully!",
            'alert-type' => 'success',
        ]);
    }

    public function search(Request $request)
    {
        $word = $request['match'];
        if (trim($word) == "") {
            $results = collect([]);
            return view('results', compact('results'));
        }

        $results = [];
        $wrap_before = '<span class="highlight">';
        $wrap_after  = '</span>';

        $pages = Page::query();
        $pages = $pages->orWhere('title', 'LIKE', '%' . $word . '%')
            ->orWhere('content', 'LIKE', '%' . $word . '%')
            ->get();

        foreach ($pages as $page) {
            $content = '';
            $matches = null;
            $title = strip_tags($page->title);
            preg_match_all('%' . $word . '%i', $title, $matches, PREG_OFFSET_CAPTURE);
            $matches = $matches[0];

            $num = count($matches);
            if ($num > 0) {
                foreach ($matches as $match) {
                    $title = preg_replace("/($match[0])/i", "$wrap_before$1$wrap_after", $title);
                }

                $results[$page->slug]['title'] = $title;
                $results[$page->slug]['category'] = $page->category;
            }

            $content = '';
            $matches = null;
            $page_contents = strip_tags($page->content);
            preg_match_all('%' . $word . '%i', $page_contents, $matches, PREG_OFFSET_CAPTURE);
            $matches = $matches[0];

            $num = count($matches);
            if ($num > 0) {
                $length = 200 / ($num >= 2 ? 2 : $num);
                $i = 0;
                $last_pos = -1;
                foreach ($matches as $match) {
                    if ($i > 1) {
                        break;
                    }

                    $pos = $match[1];
                    if ($last_pos > 0 && $pos < $last_pos + $length) {
                        continue;
                    }

                    $start = $pos - $length > 0 ? $pos - $length : 0;

                    $content = $content . '...' . substr($page_contents, $start, $length * 2) . '...    ';
                    $i++;
                    $last_pos = $pos;
                }

                $content = preg_replace("/($match[0])/i", "$wrap_before$1$wrap_after", $content);

                $results[$page->slug]['content'] = $content;
                $results[$page->slug]['category'] = $page->category;

                if (!isset($results[$page->slug]['title'])) {
                    $results[$page->slug]['title'] = $page->title;
                }
            }
        }
        $results = collect($results);

        // $pages = Page::where('title', 'LIKE', '%' . $word . '%')->get();

        // $results_2 = [];
        // foreach ($pages as $page) {
        //     $results[$page->slug] = [
        //         'title' => $page->title
        //     ];
        // }
        // $results_2 = collect($results_2);

        // $results = $results->union($results_2);

        return view('results', compact('results', 'word'));
    }
}
