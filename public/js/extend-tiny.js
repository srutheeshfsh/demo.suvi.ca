function tinymce_setup_callback(editor) {
    editor.remove();
    editor = null;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    tinyMCE.baseURL = "/vendor/tcg/voyager/tinymce/";
    tinymce.suffix = '.min';
    tinymce.init({
        menubar: false,
        selector: 'textarea.richTextBox',
        skin_url: $('meta[name="assets-path"]').attr('content') + '?path=js/skins/voyager',
        min_height: 600,
        resize: 'vertical',
        plugins: 'link, image, imagetools, code, table, textcolor, lists, media, template, paste',
        valid_elements : '*[*]',
        valid_children : '+a[div|img]',
        extended_valid_elements: '*[*],i[class],input[id|name|value|type|class|style|required|placeholder|autocomplete|onclick],a[*]',
        file_browser_callback: function (field_name, url, type, win) {
            if (type == 'image') {
                $('#upload_file').trigger('click');
            }
        },
        video_template_callback: function (data) {
            return '<video style="width: 100%; object-fit: cover;" disablePictureInPicture autoplay="autoplay" loop="loop" muted="muted" playsinline="" controlslist="nodownload"' + (data.poster ? ' poster="' + data.poster + '"' : '') + '>\n' + '<source src="' + data.source1 + '"' + (data.source1mime ? ' type="' + data.source1mime + '"' : '') + ' />\n' + (data.source2 ? '<source src="' + data.source2 + '"' + (data.source2mime ? ' type="' + data.source2mime + '"' : '') + ' />\n' : '') + '</video>';
        },
        paste_as_text: true,
        paste_preprocess: function (plugin, args) {
            args.content = args.content.replaceAll('<br />', "</p><p>");
        },
        media_alt_source: false,
        media_poster: false,
        media_live_embeds: false,
        media_dimensions: false,
        entity_encoding: 'raw',
        allow_html_in_named_anchor: true,
        verify_html: false,
        cleanup: false,
        apply_source_formatting : false,
        force_br_newlines: false,
        force_p_newlines: true,
        forced_root_block: 'p',
        toolbar: 'styleselect bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist outdent indent | fontsizeselect link image media table | code',
        convert_urls: false,
        image_caption: true,
        image_title: true,
        imagetools_toolbar: 'editimage imageoptions | alignleft aligncenter alignright',
        content_css: "/css/bootstrap.min.css,/css/slick.css,/css/fontawesome-all.min.css,/css/themify-icons.css,/css/animate.min.css,/css/magnific-popup.css,/css/meanmenu.css,/css/default.css,/css/style.css,/css/responsive.css,/css/owl.carousel.min.css",
    });
}
