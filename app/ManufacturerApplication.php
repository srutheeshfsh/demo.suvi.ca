<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ManufacturerApplication extends Model
{
    protected $guarded = [
        'id',
        'approved',
        'created_at',
        'updated_at'
    ];
}
