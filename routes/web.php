<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Barryvdh\DomPDF\Facade as PDF;

Route::get('/linkstorage', function () {
    Artisan::call('storage:link');
    return true;
});


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index')->name('home');
Route::get('/results', 'HomeController@results')->name('results');
Route::get('/specialty-use-vehicle-incentive-program', 'HomeController@specialUtilityVehicleIncentiveProgram')->name('special-utility-vehicle-incentive-program');
Route::get('/program-guide-and-eligibility-criteria', 'HomeController@programGuideAndEligibilityCriteria')->name('program-guide-and-eligibility-criteria');

Route::get('/rebate', 'HomeController@rebate')->name('rebate');
Route::get('/rebate/{slug}', 'HomeController@rebateDetail')->name('rebate-detail');

Route::get('/stories-and-events', 'HomeController@storiesAndEvents')->name('stories-and-events');
Route::get('/success-story/{slug}', 'HomeController@successStory')->name('success-story');

Route::get('/search', 'HomeController@search')->name('search');

Route::get('/faq', 'HomeController@faq')->name('faq');
Route::get('/apply', 'HomeController@apply')->name('apply');
// Route::get('/eligible-vehicles', 'HomeController@vehicles')->name('vehicles');
Route::get('/eligible-vehicles', 'FilterController@index')->name('vehicles');
Route::post('/eligible-vehicles', 'FilterController@index')->name('vehicles');
Route::get('/select-category', 'HomeController@categories')->name('categories');

Route::get('/oem-application', 'HomeController@addVehicle')->name('vehicle.add');
Route::post('/add-vehicle', 'HomeController@addVehicle')->name('vehicle.apply');
Route::post('/new-section', 'HomeController@newSection')->name('new-section');
Route::get('/export-vehicle', 'FilterController@index')->name('vehicle.export');

Route::post('/get-events', 'HomeController@getEvents')->name('get-events');


Auth::routes(['register' => false, 'login' => false]);

Route::group(['prefix' => env('ADMIN_PREFIX', 'admin')], function () {
    Route::middleware('admin')->group(function () {
        Route::post('/import', 'HomeController@import')->name('import');
    });

    Voyager::routes();
});
