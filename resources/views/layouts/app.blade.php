<!doctype html>
<html class="no-js" lang="en-ca">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
     @yield('head') 
    @if(Route::currentRouteName() == 'home')
    <title>Specialty Use Vehicle Incentive Program | Electric Vehicle Incentive Program</title>
    <meta name="description" content="The SUVI Program provides rebates on zero-emission motorcycles, cargo e-bikes, utility trucks, buses and more.">
    @elseif(Route::currentRouteName() == 'rebate')
     <title>Electric Vehicle Rebate | Specialty Use Vehicle Incentive Program</title>
    <meta name="description" content="To qualify for the full value of the rebate, a minimum 36-month lease is required for non-fleet applicants.">
    @elseif(Str::contains(Route::currentRouteName(), 'faq'))
     <title>Frequently Asked Questions on Specialty Use Vehicle Incentive Program</title>
    <meta name="description" content="In our Frequently Asked Questions (FAQ) page, you can find answers to questions on specialty use vehicle incentive program.">
    @elseif(Str::contains(Route::currentRouteName(), 'apply'))
     <title>Apply for Specialty Use Vehicle Incentive Program | Rebate Applications</title>
    <meta name="description" content="Apply for Specialty Use Vehicle Incentive Program (SUVI), after carefully reviewing the checklist document and reading the instructions.">
    @elseif(Str::contains(Route::currentRouteName(), 'vehicle.add'))
     <title>Specialty Use Vehicle Incentive Eligibility Application Form for Manufacturers</title>
    <meta name="description" content="Find the application form for Specialty-Use Vehicle Incentive (SUVI) Eligibility for manufacturers in British Columbia.">
    @elseif(Str::contains(Request::url(), 'rebates-for-tourism-industry'))
    <title>Electric Vehicles Rebates for Tourism Industry in BC</title>
    <meta name="description" content="Find details on how to get electric vehicles rebates for tourism industry in BC. You can also download the program guide.">
    @elseif(Str::contains(Request::url(), 'program-guide-and-eligibility-criteria'))
    <title>Program Guide and Eligibility Criteria – Electric Vehicle Incentive</title>
    <meta name="description" content="Find the program guide and details of eligibility criteria for CleanBC Go Electric passenger vehicle rebates.">
    @elseif(Str::contains(Request::url(), 'specialty-use-vehicle-incentive-program'))
    <title>Specialty Use Vehicle Incentive Program– SUVI Program for BC</title>
    <meta name="description" content="The SUVI Program is designed to support the adoption of ZEVs in a variety of applications including zero-emission motorcycles, and a variety of medium- and heavy-duty vehicles.">
    @elseif(Str::contains(Request::url(), 'rebates-for-fleets-and-organizations'))
    <title>Rebates for Fleets and Organizations – Rebates for Zero Emissions</title>
    <meta name="description" content="Businesses and other organizations such as non-profits and public sector organizations may apply for rebates on zero-emission motorcycles, and other utility vehicles.">
    @elseif(Str::contains(Request::url(), 'vancouver-airport-authority-s-electric-buses'))
    <title>Vancouver Airport Authority’s Electric Buses – SUVI Program</title>
    <meta name="description" content="Vancouver Airport Authority plans to electrify their light duty fleet to the greatest extent possible and are looking at options to electrify the heavy duty and bus fleet.">
    @elseif(Str::contains(Request::url(), 'stories-and-events'))
    <title>Stories and Events – Specialty Use Vehicle Incentive</title>
    <meta name="description" content="Read the success stories and find more information about events related to specialty use vehicle incentive program.">
    @elseif(Str::contains(Request::url(), 'rebate/rebates-for-individuals'))
    <title>Rebates for Individuals – SUVI Program BC</title>
    <meta name="description" content="Individuals may apply for rebates when purchasing eligible zero-emission motorcycles and neighbourhood zero-emission vehicles.">
    @elseif(Str::contains(Request::url(), 'select-category'))
    <title>Select Vehicle Category and Know the Rebate – SUVI Program</title>
    <meta name="description" content="Select the category of your vehicle and know more information about the rebate in specialty use vehicle incentive program in BC.">
    @elseif(Str::contains(Request::url(), 'success-story/district-of-summerland-s-electric-zamboni'))
    <title>District of Summerland’s Electric Zamboni – Suvi BC</title>
    <meta name="description" content="The District of Summerland made the decision to purchase an electric lithium ion machine versus replacing with the traditional propane re-surfacer or the lead acid electric version.">
    @elseif(Str::contains(Request::url(), 'success-story/school-district-62-puts-electric-school-buses-on-the-road'))
    <title>School District 62 Puts Electric School Buses on the Road</title>
    <meta name="description" content="Sooke School District began using the first electric school bus in BC and it is purchased with support from the CleanBC Specialty Use Vehicle Incentive program.">
   
    @endif
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" type="image/x-icon" href="/img/favicon.png?v=1">
    <link rel="canonical" href="{{ url()->current() }}">
    <link rel="alternate" href="{{ url()->current() }}" hreflang="en-ca">
    <!-- Place favicon.png in the root directory -->

    <!-- CSS here -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/slick.css">
    <link rel="stylesheet" href="/css/fontawesome-all.min.css" media="print" onload="this.media='all'">
    <link rel="stylesheet" href="/css/themify-icons.css" media="print" onload="this.media='all'">
    <link rel="stylesheet" href="/css/animate.min.css" media="print" onload="this.media='all'">
    <link rel="stylesheet" href="/css/aos.css" media="print" onload="this.media='all'">
    <link rel="stylesheet" href="/css/magnific-popup.css" media="print" onload="this.media='all'">
    <link rel="stylesheet" href="/css/meanmenu.css" media="print" onload="this.media='all'">
    <link rel="stylesheet" href="/css/default.css" media="print" onload="this.media='all'">
    <link rel="stylesheet" href="/css/style.css?v=1.232">
    <link rel="stylesheet" href="/css/responsive.css" media="print" onload="this.media='all'">
    <link rel="stylesheet" href="/css/owl.carousel.min.css" media="print" onload="this.media='all'">
    @yield('styles')

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Google Tag Manager -->
    <script>
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NVXTG68');
    </script>
    <!-- End Google Tag Manager -->
</head>

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NVXTG68" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- preloader start -->
    <div id="preloader">
        <div class="preloader">
            <span></span>
            <span></span>
        </div>
    </div>
    <!-- preloader end  -->

    <!-- header start -->
    <header class="main-header fixed-top">
        <div class="header-top-area pb-1">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-xl-2 col-lg-4 pt-3">
                        <div class="logo d-none d-lg-block">
                            <a href="{{ route('home') }}"><img src="/img/logo/logo.png" alt="logo"></a>
                        </div>
                        <div class="logo-mobile d-none">
                            <a href="{{ route('home') }}"><img src="/img/logo/logo-tp.png" alt="logo"></a>
                        </div>
                    </div>
                    <div class="col align-self-end">
                        <div class="header-top-right f-right">
                            <div class="header-btn">
                                <a href="{{ route('apply') }}" data-animation="fadeInLeft" data-delay=".6s"
                                    class="thm-btn thm-btn-2">Apply</a>
                            </div>
                        </div>
                        <div class="mobile-menu"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-bottom-area">
            <div class="header-bottom-wrapper">
                <div class="container-fluid">
                    <div class="row align-items-center">
                        <div class="col-lg-8">
                            <div class="main-menu">
                                <nav id="mobile-menu">
                                    <ul>
                                        <li class="{{ Route::currentRouteName() == 'home' ? 'active' : '' }}"><a
                                                href="{{ route('home') }}">Home</a></li>
                                        <li
                                            class="{{ Str::contains(Route::currentRouteName(), 'rebate') ? 'active' : '' }}">
                                            <a href="{{ route('rebate') }}">Rebates</a></li>
                                        @if(setting('site.success-stories-activation', false))
                                        <li
                                            class="dropdown {{ Route::currentRouteName() == 'stories-and-events' ? 'active' : '' }}">
                                            <a href="{{ route('stories-and-events') }}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Stories & Events</a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="{{ route('stories-and-events') }}#stories">Stories</a></li>
                                                <li><a href="{{ route('stories-and-events') }}#eventssection">Events</a></li>
                                            </ul>
                                        </li>
                                        @endif
                                        <li
                                            class="{{ Route::currentRouteName() == 'vehicles' || Route::currentRouteName() == 'categories' ? 'active' : '' }}">
                                            <a href="{{ route('categories') }}"> Eligible Vehicles </a></li>
                                        <li class="{{ Route::currentRouteName() == 'faq' ? 'active' : '' }}"><a
                                                href="{{ route('faq') }}">FAQs</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <form action="{{ route('search') }}" method="GET">
                                <div class="input-group">
                                    <input type="text" name="match" class="form-control form-control-sm"
                                        value="{{ isset($word) ? $word : '' }}">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary btn-sm" type="submit"><i
                                                class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- header end -->

    @yield('content')
    <!-- footer start -->
    <footer class="footer-area footer-height" data-background="/img/bg/footer-bg.jpg">
        <div class="footer-top pt-120">
            <div class="container">
                <div class="row justify-content-between mb-3">
                    <div class="col-md-4 p-0">
                        <div class="logo mb-15 p-2">
                            <a href="#"><img src="/img/logo/logo.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-4 p-0">
                        <div class="logo-suvi p-2 mb-15">
                            <a href="#"><img src="/img/logo/suvi-logo-trans1.png" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4">
                        <div class="footer-widget mb-40" data-aos="fade-up" data-aos-delay="100">

                            <div class="footer-text mb-25">
                                @php
                                $section = App\Section::where('name', 'Footer text')->first()
                                @endphp
                                @if ($section)
                                {!! $section->content !!}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-4 offset-xl-1 ">
                        <div class="footer-widget link mb-40" data-aos="fade-up" data-aos-delay="300">
                            <!-- <h3>Fundraise For</h3> -->
                            <ul class="footer-link">
                                <li><a href="{{ route('home') }}">Home</a></li>
                                <li><a href="{{ route('rebate') }}">Rebates</a></li>
                                @if(setting('site.success-stories-activation', false))
                                <li><a href="{{ route('stories-and-events') }}">Stories & Events</a></li>
                                @endif
                                <li><a href="{{ route('categories') }}"> Eligible Vehicles </a></li>
                                <li><a href="{{ route('apply') }}">Apply</a></li>
                                <li><a href="{{ route('faq') }}">FAQs</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4">
                        <div class="footer-widget link mb-40 pl-2" data-aos="fade-up" data-aos-delay="500">
                            <!-- <h3>Resources</h3> -->
                            <p class="color-text-white">For more information, please contact<br><span class="color-text-yellow"><a class="color-text-yellow" href="mailto:suvi@pluginbc.ca">suvi@pluginbc.ca</a></span></p>
                            
                            <div class="social_icons mt-5">
                                 <p>
                                <a href="mailto:suvi@pluginbc.ca" target="_blank"><i class="fa fa-envelope"></i></a> 
                                 <a href="https://www.facebook.com/SUVIBC" target="_blank"><i class="fab fa-facebook"></i></a> 
                                 <a href="https://www.instagram.com/suvi_bc/" target="_blank"><i class="fab fa-instagram"></i></a> 
                                 <a href="https://twitter.com/suvi_bc/" target="_blank"><i class="fab fa-twitter-square"></i></a> 
                                 <a href="https://www.linkedin.com/company/plug-in-bc/" target="_blank"><i class="fab fa-linkedin"></i></a> 
                                 </p>
                             </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="copyright-text text-center">
                            <p>Copyright By @ FSHDesign - 2022</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer end -->

    <!-- JS here -->
    <script src="/js/vendor/modernizr-3.8.0.min.js"></script>
    <script src="/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="/js/popper.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/slick.min.js"></script>
    <script src="/js/waypoints.min.js"></script>
    <script src="/js/jquery.counterup.min.js"></script>
    <script src="/js/aos.js"></script>
    <script src="/js/jquery.scrollUp.min.js"></script>
    <script src="/js/jquery.meanmenu.min.js"></script>
    <script src="/js/jquery.magnific-popup.min.js"></script>
    <script src="/js/plugins.js"></script>
    <script src="/js/owl.carousel.min.js"></script>
    <script src="/js/main.js"></script>
    @yield('scripts')
</body>

</html>
