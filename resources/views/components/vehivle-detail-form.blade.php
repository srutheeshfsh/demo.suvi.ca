<tbody class="vehicle-detail" id="vehicle_{{ $index }}">
    <tr>
        <th>
            Category
        </th>
        <td>
            <select type="text" name="vehicle_category[]" class="vehicle_category" required>
                <option value="" selected>Select Category</option>
                @foreach (App\EligibleCategory::all() as $category)
                <option {{ old("vehicle_category.$index")==$category->name ? "selected" : "" }}>
                    {{ $category->name }}</option>
                @endforeach
            </select>
        </td>
    </tr>
    <tr>
        <th class="vehicle_type_text">
            Type
        </th>
        <td>
             @php  
            $vehicles = App\Vehicle::where('category','On road Medium and Heavy-Duty')->groupBy('vehicle_type')->pluck('vehicle_type');
             @endphp
                                 <select id="vehicle_type" name="vehicle_type[]" class="form-control select2 d-none" required>
                                     <option > Vehicle Class</option>
                                     <option value="Class 2B"> Class 2B</option>
                                     <option value="Class 3"> Class 3</option>
                                     <option value="Class 4"> Class 4</option>
                                     <option value="Class 5"> Class 5</option>
                                     <option value="Class 6"> Class 6</option>
                                     <option value="Class 7"> Class 7</option>
                                     <option value="Class 8"> Class 8</option>
                                    
                                    <!--@foreach ($vehicles   as $vehicle_type)-->
                                    <!-- <option value="{{ $vehicle_type }}">{{ $vehicle_type }}</option>-->
                                    <!--@endforeach-->
                                </select>
            <input type="text" name="vehicle_type[]" value="{{ old("vehicle_type.$index") }}"
                placeholder="Vehicle Type:" required>
        </td>
    </tr>
    <tr>
        <th>
            Make
        </th>
        <td>
            <input type="text" name="vehicle_make[]" value="{{ old("vehicle_make.$index") }}"
                placeholder="Vehicle Make:" required>
        </td>
    </tr>
    <tr>
        <th>
            Model
        </th>
        <td>
            <input type="text" name="vehicle_model[]" value="{{ old("vehicle_model.$index") }}"
                placeholder="Vehicle Model:" required>
        </td>
    </tr>
    <tr>
        <th>
            Year
        </th>
        <td>
            <input type="text" name="model_year[]" value="{{ old("model_year.$index") }}" placeholder="Model Year:"
                required>
        </td>
    </tr>
    <tr>
        <th>
            Battery Size
        </th>
        <td>
            <input type="text" name="vehicle_battery_size[]" value="{{ old("vehicle_battery_size.$index") }}"
                placeholder="Battery Size/Cargo Volume and weight capacity in case of E-Bikes: (e.g. 400Wh / 300L / 200kg)"
                required>
        </td>
    </tr>
    <tr>
        <th>
            Fuel Type
        </th>
        <td>
            <select type="text" name="vehicle_fuel_type[]" required>
                <option value="" selected>Select Fuel Type</option>
                <option {{ old("vehicle_fuel_type.$index")=="Battery Electric Vehicle (BEV)" ? "selected" : "" }}>
                    Battery Electric Vehicle(BEV)</option>
                <option {{ old("vehicle_fuel_type.$index")=="Hydrogen Fuel Cell Vehicle (FCV)" ? "selected" : "" }}>
                    Hydrogen Fuel Cell Vehicle (FCV)</option>
                <option {{ old("vehicle_fuel_type.$index")=="Plug-in Hybrid Electric Vehicle (PHEV)" ? "selected" : ""
                    }}>
                    Plug-in Hybrid Electric Vehicle (PHEV)</option>
            </select>
        </td>
    </tr>
    <tr>
        <th>
            MSRP (CA$)
        </th>
        <td>
            <input type="number" step="0.01" min="0" name="msrp[]" value="{{ old("msrp.$index") }}"
                placeholder="MSRP: (in CAD)" required>
        </td>
    </tr>
    <tr>
        <th>
            Image
        </th>
        <td>
            <input type="file" name="image[]" accept=".jpeg, .jpg, .png" placeholder="Image" required>
            <p><b>*Actual image of the vehicle (size less than 1MB, aspect ratio
                    1:1, formats jpg or
                    png) </b></p>
        </td>
    </tr>
    @if($index > 0)
    <tr>
        <th colspan="2">
            <button class="btn btn-danger remove" type="button" data-id="{{ $index }}">Remove</button>
        </th>
    </tr>
    @endif
</tbody>
