@extends('layouts.app')

@section('content')
<main>
    <section class="faq-area pt-200 pb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 offset-lg-1 offset-xl-1 col-md-12 col-xl-10 pt-5" data-aos="fade-up"
                    data-aos-delay="100">
                    @foreach ($results as $key => $result)
                    <div class="card">
                        <div class="card-body">
                            <div class="card-title">
                                <a
                                    href="{{ $result['category'] == 'none' ? '/' . $key : '/' . $result['category'] . '/' . $key }}">{!!
                                    $result['title'] !!}</a>
                                <small>{{ $result['category'] == 'none' ? '' : ' - ' . Str::title(str_replace('-', ' ', $result['category'])) }}</small>
                            </div>
                            @if (isset($result['content']))
                            {!! $result['content'] !!}
                            @endif
                        </div>
                    </div>
                    @endforeach
                    @if (count($results) == 0)
                    No results
                    @endif
                </div>
            </div>
        </div>
    </section>
</main>
@endsection
