<section class="counter-area categories py-5 gray-bg"> {{-- data-overlay="8" data-background="/img/bg/about-bg.jpg" --}}
    <div class=" d-flex">
        <div class="row col-md-12 mx-auto dark-bg p-2 ">
            <div class="col-md-12 text-center mb-50 mt-5">
                <h2 class="color-text-blue">Eligible Vehicles Category</h2>
            </div>
            <div class="row col-md-11 mx-auto">   
            @foreach (App\EligibleCategory::all() as $category)
            <div class="col-lg-4 col-sm-12  row row-eq-height mb-50 mx-auto">
                <!--<div class=" col w-90 card text-center p-3 mb-10">-->
                <!--    <div class="card-title">-->
                <!--        <a data-toggle="collapse" href="#c{{ $loop->iteration }}">-->
                <!--            <h4 class="color-text-greendouglous">{{ $category->name }}</h4>-->
                <!--            <i class="fa fa-angle-double-down" aria-hidden="true" style="position: relative; top:26px"></i>-->
                <!--        </a>-->
                <!--    </div>-->
                <!--    <div class="card-body color-text-deepblue collapse" id="c{{ $loop->iteration }}">-->
                <!--        {!! nl2br(e($category->description)) !!}-->

                <!--        @if ($category->has_popup)-->
                <!--        <br>-->
                <!--        <a href="javascript:;" data-toggle="modal" data-target="#m{{ $category->id }}">-->
                <!--            Read More-->
                <!--        </a>-->
                <!--        @endif-->
                <!--    </div>-->
                <!--    <hr>-->
                <!--    {!! $category->footer !!}-->
                <!--</div>-->
                 <div class=" col w-90 card text-center p-3 mb-10">
                        <div class="card-title">
                            <a data-toggle="collapse" href="#c{{ $loop->iteration }}">
                                <h3 class="color-text-dark-greendouglous">{{ $category->name }}</h3>
                                <span class="color-text-dark-greendouglous-sub">Rebate Up To</span>
                                {{-- <i class="fa fa-angle-double-down" aria-hidden="true" style="position: relative; top:26px"></i> --}}
                            </a>
                        </div>
                        <div class="card-body p-0">
                            {!! $category->description !!}
                        </div>
                        <hr>
                        <div class="catd-footer p-2">
                            @if ($category->has_popup)
                                <br>
                                <a href="javascript:;" data-toggle="modal" class="read_more"
                                    data-target="#m{{ $category->id }}">
                                    Read More
                                </a>
                            @endif
                        </div>
                        {{-- {!! $category->footer !!} --}}
                    </div>
            </div>
            @endforeach
            </div> 
        </div>
    </div>
</section>

@foreach (App\EligibleCategory::where('has_popup', 1)->get() as $category)
<!-- Modal -->
<div class="modal fade eligible-category" id="m{{ $category->id }}" tabindex="-1" role="dialog" aria-hidden="true">
    <!--<div class="modal-dialog modal-dialog-centered" role="document">-->
    <!--    <div class="modal-content">-->
    <!--        <div class="modal-header">-->
    <!--            <h5 class="modal-title" id="exampleModalLabel">{{ $category->name }}</h5>-->
    <!--            <button type="button" class="close" data-dismiss="modal" aria-label="Close">-->
    <!--                <span aria-hidden="true">&times;</span>-->
    <!--            </button>-->
    <!--        </div>-->
    <!--        <div class="modal-body">-->
    <!--            {!! $category->popup_content !!}-->
    <!--        </div>-->
    <!--        <div class="modal-footer">-->
    <!--            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</div>-->
     <div class="modal-dialog modal-dialog-centered text-center" role="document">
            <div class="modal-content">
                 <div class="modal-header row d-inline-flex text-center">
                    <div class="1 col-md-12 d-flex">
                    <h2 class="modal-title color-text-dark-greendouglous mx-auto" id="exampleModalLabel">{{ $category->name }} </h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="2 col-md-12"> <p class="color-text-dark-greendouglous-sub">Rebate Up To</p></div>
                </div>
                <div class="modal-body">
                     <br>
                    {!! $category->popup_content !!}
                </div>
              
            </div>
        </div>
</div>
@endforeach
