<?php

namespace App\Imports;

use App\Vehicle;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;

class VehicleImport implements ToCollection, WithHeadingRow, WithEvents, WithCalculatedFormulas
{
    private $current_sheet_name;

    public function __construct()
    {
        $this->current_sheet_name = '';
    }

    public function collection(Collection $rows)
    {
        $current_category = '';
        $is_archived = $this->current_sheet_name == 'Archived Models';

        foreach ($rows as $row) {
            if ($row['year'] == null) {
                $current_category = $row['make'];
            } else {
                $image = isset($row['image']) ? trim($row['image']) : '';

                $vehicle = Vehicle::updateOrCreate([
                    'make' => trim($row['make']),
                    'year' => trim($row['year']),
                    'model' => trim($row['model']),
                    'fuel_type' => trim($row['fuel_type']),
                    'battery_size' => trim($row['battery_size']),
                ], [
                    'incentive' => trim($row['incentive_maximum']),
                    'base_msrp' => trim($row['base_msrp']),
                    'vehicle_type' => isset($row['vehicle_type']) ? trim($row['vehicle_type']) : null,
                    'category' => trim($current_category),
                    'is_archived' => $is_archived,
                ]);

                if ($vehicle->image = null) {
                    $vehicle->image = $image;
                    $vehicle->save();
                }
            }
        }
    }

    public function registerEvents(): array
    {
        return [
            BeforeSheet::class => function (BeforeSheet $event) {
                $this->current_sheet_name = $event->getSheet()->getDelegate()->getTitle();
            }
        ];
    }

    public function headingRow(): int
    {
        return 3;
    }
}
