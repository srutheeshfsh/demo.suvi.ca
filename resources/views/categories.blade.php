@extends('layouts.app')

@section('content')
<main>
    {{-- <!--page title start -->
    <section class="page-title-area bg-overly slider-area slider-2" data-overlay="5"
        data-background="/img/bg/vehicle_bg.png">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">

                    <div class="page-title text-center" data-aos="fade-up" data-aos-delay="100">
                        <h1>Select Category</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb justify-content-center">
                                <li class="breadcrumb-item">
                                    <a href="{{ route('home') }}">Home</a>
                                </li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section> --}}
    <!-- page title end -->
    <section class="causes-area gray-bg pt-200 pb-90">
        <div class="container">
            <div class="section-title text-center" data-aos="fade-up" data-aos-delay="100">
                <div class="bg-title">
                    <!-- <h1>Casuses</h1> -->
                </div>
                <!-- <h5>Good Causes</h5> -->
                <h2 class="color-text-greendouglous">Select the category</h2>
                {{-- <p class="color-text-deepblue"> The SUVI Program provides rebates on zero-emission motorcycles, cargo
                e-bikes, utility trucks, buses and more.
            </p> --}}
            </div>
            <div class="row">
                @foreach (App\EligibleCategory::all() as $category)
                <div class="col-md-4 col-sm-12">
                    <a href="{{ route('vehicles', ['category' => $category->name]) }}">
                        <div class="mb-30">
                            <div class="causes-thumb">
                                <img src="{{ Voyager::image($category->image) }}" alt="">
                            </div>
                            <div class="causes-content text-center">
                                <div class="causes-head causes-head-3 clearfix mt-3 mb-10">
                                    <h3 class="category-title">{{ $category->name }}</h3>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </section>
</main>
@endsection
