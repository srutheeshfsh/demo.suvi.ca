<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>OEM Application Form</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSS here -->
    <link rel="stylesheet" href="{{ public_path(" css/bootstrap.min.css") }}">
    {{--
    <link rel="stylesheet" href="/vendor/bootstrap/css/bootstrap.min.css"> --}}

    <style>
        table {
           border-collapse: collapse;
          }
        .underline {
            border-bottom: 1px solid;
            width: 100%;
            /* display: inline-block; */
        }


        .table-borderless {
            width: 100%;
            border: none;
        }


        td {
            width: 15%;
        }

        td.underline {
            width: 35%;
        }

        body {
            font-size: 14px
        }
        
        table.vehicle td, table.vehicle th {
            border: 1px solid #000;
            padding: 8px;
        }
        table.vehicle tr > td {
            border-bottom: 1px solid #000000;
        }

        @page {
            margin: 12px;
        }
    </style>

</head>

<body>
    <div class="container">
        <h4 class="text-center">BRITISH COLUMBIA - SPECIALTY-USE VEHICLE INCENTIVE (SUVI) VEHICLE ELIGIBLITY
            APPLICATION FORM FOR
            MANUFACTURERS</h4>

        <p class="mt-3">The purpose of this form is to assist the BC Ministry of
            Energy, Mines and Low Carbon Innovation in assessing whether new Zero-emission
            vehicle models can become eligible for SUVI under the Go Electric Vehicle Incentive
            Program. Original equipment manufacturers (OEMs)
            should fill out this form to submit vehicle models for consideration.</p>

        <hr>

        <table class="table-borderless">
            <tbody>
                <tr>
                    
                    <td>Date:</td>
                    <td class="underline">{{ Carbon\Carbon::parse($form->created_at)->format('M d, Y') }}
                    </td>
                    <td>Company/Manufacture:</td>
                    <td class="underline">{{ $form->manufacturer_name }}</td>
                </tr>
                <tr>
                    <td>Contact Number:</td>
                    <td class="underline">{{ $form->contact_phone }}</td>
                    <td>Representative Name:</td>
                    <td class="underline">{{ $form->representitive_name }}</td>
                   
                </tr>
                <tr>
                    <td>Email:</td>
                    <td class="underline">{{ $form->contact_email }}</td>
                    <td>Title/Position:</td>
                    <td class="underline">{{ $form->representitive_position }}</td>
                </tr>
            </tbody>
        </table>
        <hr>

        <p><b>PART A</b>: For vehicle models currently on the list of eligible vehicles (addition of a new model year).
            Please
            initial if the following statement is true.</p>

        <p><u>&nbsp;&nbsp;&nbsp;{{ $form->sign }}&nbsp;&nbsp;&nbsp;</u> I certify that the vehicles included on the
            current list of
            eligible vehicles have not been modified from
            the vehicles that were previously approved by
            the Ministry of Energy, Mines and Low Carbon Innovation for inclusion on the List of Eligible Vehicles
            including
            warranty and after sales service provisions.</p>

        <p><b>PART B</b>: For vehicle models not currently on the list of eligible vehicles. Please complete Table 1</p>
        <b class="text-center" style="display: block">Table 1 – New Vehicle application for specialty-use vehicle
            incentive program
            eligibility</b>
            <br>
        <!--<table class="table table-bordered border-1" style="border-collapse: collapse;">-->
        <!--    <thead>-->
        <!--        <tr style="border:1px solid #000;">-->
        <!--            <th style="border:1px solid #000;">Vehicle Category</th>-->
        <!--            <th style="border:1px solid #000;">Vehicle Type</th>-->
        <!--            <th style="border:1px solid #000;">Model Year</th>-->
        <!--            <th style="border:1px solid #000;">vehicle Make</th>-->
        <!--            <th style="border:1px solid #000;">Vehicle Model</th>-->
        <!--            <th style="border:1px solid #000;">Battery Size</th>-->
        <!--            <th style="border:1px solid #000;">Fuel Type</th>-->
        <!--            <th style="border:1px solid #000;">Base MSRP</th>-->
        <!--        </tr>-->
        <!--    </thead>-->
        <!--    <tbody>-->
        <!--        @foreach ($forms as $form)-->
        <!--        <tr style="border:1px solid #000;">-->
        <!--            <td style="border:1px solid #000;">{{ $form->vehicle_category }}</td>-->
        <!--            <td style="border:1px solid #000;">{{ $form->vehicle_type }}</td>-->
        <!--            <td style="border:1px solid #000;">{{ $form->model_year }}</td>-->
        <!--            <td style="border:1px solid #000;">{{ $form->vehicle_make }}</td>-->
        <!--            <td style="border:1px solid #000;">{{ $form->vehicle_model }}</td>-->
        <!--            <td style="border:1px solid #000;">{{ $form->vehicle_battery_size }}</td>-->
        <!--            <td style="border:1px solid #000;">{{ $form->vehicle_fuel_type }}</td>-->
        <!--            <td style="border:1px solid #000;">{{ $form->msrp }}</td>-->
        <!--        </tr>-->
        <!--        @endforeach-->

        <!--    </tbody>-->
        <!--</table>-->
        @foreach ($forms as $form)
        <hr>
        <table class="table-borderless">
            <tbody>
                <tr>
                    <td>Vehicle Category:</td>
                    <td class="underline">{{ $form->vehicle_category }}
                    </td>
                    @if($form->vehicle_category=="On road Medium and Heavy-Duty")
                    <td>Vehicle Class :</td>
                    @else
                    <td>Vehicle Type :</td>
                    @endif
                    <td class="underline">{{ $form->vehicle_type }}</td>
                </tr>
                <tr>
                    <td>Model Year:</td>
                    <td class="underline">{{ $form->model_year }}</td>
                    <td>Vehicle Make:</td>
                    <td class="underline">{{ $form->vehicle_make }}</td>
                </tr>
                <tr>
                    <td>Vehicle Model:</td>
                    <td class="underline">{{ $form->vehicle_model }}</td>
                    <td>Battery Size:</td>
                    <td class="underline">{{ $form->vehicle_battery_size }}</td>
                </tr>
                 <tr>
                    <td>Fuel Type:</td>
                    <td class="underline">{{ $form->vehicle_fuel_type }}</td>
                    <td>Base MSRP:</td>
                    <td class="underline">{{ $form->msrp }}</td>
                </tr>
            </tbody>
        </table>
        <hr>
        <br>
        @endforeach
        
        
    </div>
</body>

</html>
