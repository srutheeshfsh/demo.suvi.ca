@extends('layouts.app')

@section('styles')
<style>
    .table input,
    .table select {
        margin-bottom: 0 !important;
        height: auto !important;
    }
</style>

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

{!! htmlScriptTagJsApi() !!}
@endsection

@section('content')
<main>
    <!-- page title start -->
    <section class="page-title-area bg-overly slider-area slider-2" data-overlay="5"
        data-background="/img/bg/success-stories-header.jpg">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">

                    <div class="page-title text-center" data-aos="fade-up" data-aos-delay="100">
                        <h1>OEM Application</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- page title end -->

    <form class="statistics-form give-form" method="POST" action="{{ route('vehicle.apply') }}" id="{{ getFormId() }}"
        enctype="multipart/form-data">
        @csrf
        <!-- about start -->
        <section class="statstics-area fix pt-50 pb-100">
            <div class="container">
                <div class="row">
                    @php
                    $section = App\Section::where('name', 'OEM description')->first()
                    @endphp
                    @if ($section)
                    {!! $section->content !!}
                    @endif
                    <div class="col-xl-12 col-lg-12 mt-5">
                        <div class="row" data-aos="fade-up" data-aos-delay="100">

                            <div class="col-xl-12 col-lg-12">
                                <input type="text" name="manufacturer_name" value="{{ old('manufacturer_name') }}"
                                    placeholder="Company/Manufacture:" required>
                            </div>
                            <div class="col-xl-12 col-lg-12">
                                <input type="text" name="representitive_name" value="{{ old('representitive_name') }}"
                                    placeholder="Representative Name:" required>
                            </div>
                            <div class="col-xl-12 col-lg-12">
                                <input type="text" name="representitive_position"
                                    value="{{ old('representitive_position') }}" placeholder="Title/Position:" required>
                            </div>
                            {{-- <div class="col-xl-12 col-lg-12">
                                <input type="text" name="name" placeholder="Name:">
                            </div>
                            <div class="col-xl-12 col-lg-12">
                                <input type="text" name="contact" placeholder="Contact:">
                            </div> --}}
                            <div class="col-xl-12 col-lg-12">
                                <input type="email" name="contact_email" value="{{ old('contact_email') }}"
                                    placeholder="Email:" required>
                            </div>
                            <div class="col-xl-12 col-lg-12">
                                <input type="text" name="contact_phone" value="{{ old('contact_phone') }}"
                                    placeholder="Phone Number:" required>
                            </div>
                            <div class="col-xl-12 col-lg-12">
                                <p class="pb-4 pt-4"><span class="color-text-yellow">PART A:</span> <span
                                        class=" color-text-greendouglous">For vehicle models currently on the list of
                                        eligible vehicles (addition of a new model year). Please initial if the
                                        following statement
                                        is true.</span></p>

                                <p class="lineInput pb-4"><input type="text" class=" input" id="usr" name="sign"
                                        required>I certify that the vehicles included on the current list of eligible
                                    vehicles have not been modified from the vehicles that were
                                    previously approved by the Ministry of Energy, Mines and Low Carbon Innovation for
                                    inclusion on the List of Eligible Vehicles including
                                    warranty and after sales service provisions.
                                </p>

                                <p class="pb-4"><span class="color-text-yellow">PART B:</span> <span
                                        class=" color-text-greendouglous"> For vehicle models not currently on the list
                                        of eligible vehicles. Please complete form below.</span></p>
                            </div>
                            <div class="col-xl-12 col-lg-12">
                                <div class="table-responsive">
                                    <table class="table" id="vehicles">
                                        @if(old('vehicle_category'))
                                        @foreach (old('vehicle_category') as $index => $value)
                                        @include('components.vehivle-detail-form', ['index' => $index])
                                        @endforeach
                                        @else
                                        @include('components.vehivle-detail-form', ['index' => 0])
                                        @endif
                                    </table>
                                </div>
                            </div>
                            <div class="col-xl-12 col-lg-12">
                                <button class="thm-btn thm-btn-2" type="button" id="add" style="border:
                                none;">
                                    Add Another Vehicle
                                </button>
                            </div>

                            <div class="col-xl-12 col-lg-12">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="certify">
                                    <label for="certify">I certify the information I provided on and in connection with
                                        this
                                        form is true
                                        and correct to the best of my knowledge. <br>
                                        By uploading the image, I certify that I own the copyright of the image and it
                                        can be made public on this website.
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center pt-5">
                {!! htmlFormButton("Submit", ['class' => 'thm-btn thm-btn-2 submit', 'style' => 'border:
                none;', 'data-badge' => 'bottomleft'
                ]) !!}
            </div>
        </section>
        <!-- about end -->
    </form>
</main>

@if (session('message') || count($errors->all()) > 0)
<div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pb-2">
                @if (session('message'))
                <p class="text-center">{{ session('message')['message'] }}</p>
                @endif
                @foreach ($errors->all() as $error)
                <p class="text-center">{{ $error }}</p>
                @endforeach
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Got it!</button>
            </div>
        </div>
    </div>
</div>
@endif

@endsection

@section('scripts')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function () {

         $('body').on('change','.vehicle_category',function(){
            var vehicle_category = $(this).val();
            if(vehicle_category =="On road Medium and Heavy-Duty"){
                $(this).parents('.vehicle-detail').find('.vehicle_type_text').html("Class");
                $(this).parents('.vehicle-detail').find('.vehicle_type_text').parents('tr').find('input').addClass("d-none");
                $(this).parents('.vehicle-detail').find('.vehicle_type_text').parents('tr').find('select').removeClass("d-none");
            }else{
               $(this).parents('.vehicle-detail').find('.vehicle_type_text').html("Type");
               $(this).parents('.vehicle-detail').find('.vehicle_type_text').parents('tr').find('input').removeClass("d-none");
               $(this).parents('.vehicle-detail').find('.vehicle_type_text').parents('tr').find('select').addClass("d-none");
            }
          });    
        $('#certify').change(function() {
            if(this.checked) {
                $('.submit').prop('disabled', false);
            } else {
                $('.submit').prop('disabled', true);
            }
        });

        $('input:not([id=certify])').change(function() {
            if ($('#certify').prop('checked') == false) {
                $('.submit').prop('disabled', true);
            }
        });

        count = {{ old('vehicle_category') ? count(old('vehicle_category')) : 1 }}
        $('#add').on('click', function () {
            index = count;

            $.ajax({
                url: "{{ route('new-section') }}",
                type: "POST",
                data: {
                    'index': index
                },
                success: function (response) {
                    $('#vehicles').append(response);
                    count++;
                },
                error: function (jqXHR, exception) {
                }
            });
        });

        $(document).on('click', '.remove', function() {
            id = $(this).data('id');
            $("#vehicle_" + id).remove();
        });
    });
</script>

@if (session('message') || count($errors->all()) > 0)
<script>
    $('#messageModal').modal('show');
</script>
@endif
@endsection
