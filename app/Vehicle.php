<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Vehicle extends Model
{
    protected $guarded = [
        'id',
        'created_at',
        'updated_at'
    ];

    public static function categories()
    {
        return EligibleCategory::pluck('name')->toArray();
    }

    // public static $categories = [
    //     'airport-and-port-specialty-vehicle',
    //     'cargo-e-bikes',
    //     'electric-motorcycle',
    //     'low-speed',
    //     'utility',
    //     'on-road-medium-and-heavy-duty'
    // ];
}
