<section class="causes-area gray-bg pt-120 pb-90">
    <div class="container">
        <div class="section-title text-center" data-aos="fade-up" data-aos-delay="100">
            <div class="bg-title">
                <!-- <h1>Casuses</h1> -->
            </div>
            <!-- <h5>Good Causes</h5> -->
            <h2 class="color-text-greendouglous">Rebates</h2>
            @php
            $section = App\Section::where('name', 'Rebate section text')->first()
            @endphp
            @if ($section)
            {!! $section->content !!}
            @endif
        </div>
        <div class="custom-row">
            <div class="slick-control col-md-12 text-center pb-40">
                <button class="slick-prev border-0"><img src="/img/f-left.jpg"></button>
                <button class="slick-next border-0"><img src="/img/f-right.jpg"></button>
            </div>
            <div class="testimonial-active" data-aos="fade-up" data-aos-delay="100">

                @foreach (App\Page::where('category', 'rebate')->get() as $rebate)
                <div class="col-md-6 col-sm-12">
                    <div class="causes-single mb-30" data-aos="fade-up" data-aos-delay="500">
                        <div class="causes-thumb">
                            <a href="{{ route('rebate-detail', ['slug' => $rebate->slug]) }}"><img
                                    src="{{ Voyager::image($rebate->cover_image) }}" alt=""></a>
                        </div>
                        <div class="causes-content">
                            <div class="causes-head causes-head-3 clearfix mb-10">
                                <a href="{{ route('rebate-detail', ['slug' => $rebate->slug]) }}">
                                    <h4>{{ $rebate->title }}</h4>
                                </a>
                            </div>
                            <div class="causes-text mb-25">
                                <p>
                                    {!! nl2br(e($rebate->excerpt)) !!}
                                </p>
                            </div>
                            <a class="Btn-border-yell"
                                href="{{ route('rebate-detail', ['slug' => $rebate->slug]) }}">Learn
                                More <i class="ti-angle-double-right"></i></a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
