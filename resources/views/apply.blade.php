@extends('layouts.app')

@section('content')
<main>
    <!--page title start -->
    <section class="page-title-area bg-overly slider-area slider-2" data-overlay="5" data-background="/img/bg/02.jpg">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">

                    <div class="page-title text-center" data-aos="fade-up" data-aos-delay="100">
                        <h1>Apply</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- page title end -->

    <!-- faq start -->
    <section class="faq-area pt-50 pb-100">
        <div class="container">
            <div class=" section-title text-center aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
                <h3 class="color-text-greendouglous">APPLY TODAY</h3>
                <h2>You are looking for:</h2>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="faq-left mb-20 text-left" data-aos="fade-up" data-aos-delay="100">
                        <a href="{{ route('vehicle.add') }}" class="btn btn-lg heavyBtn thm-btn thm-btn-2 col px-2">OEM
                            application <br>
                            <div class="btn-subtext">*Application to add the vehicle to the eligible list</div>
                        </a>
                        <span>Please carefully review the OEM Application <b><a href="/documents/OEM Checklist for eligible vehicles (SUVI).pdf" target="_blank">checklist document</a></b> before applying.</span>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="faq-right mb-20" data-aos="fade-up" data-aos-delay="300">
                        <a href="https://pluginbc.smapply.ca/prog/SUVI/" target="_blank"
                            class="btn btn-lg heavyBtn thm-btn thm-btn-2 col px-2">Rebate
                            Application
                            <br>
                            <div class="btn-subtext">*application to apply for SUVI rebate</div>
                        </a>
                        <!--<span>Please carefully review the <b><a href="/documents/SUVI checklist.pdf" target="_blank">checklist document</a></b> before applying.</span> -->
                        <span>Please carefully review the Rebate Application <b><a href="/documents/SUVI checklist.pdf" target="_blank">checklist document</a></b> before applying.</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- faq end -->

    <!-- feature start -->
    <section class="feature-area color-white pb-90">
        <div class="container">
            <div class="section-title text-center" data-aos="fade-up" data-aos-delay="100">
                <div class="bg-title">
                    <!-- <h1>Features</h1> -->
                </div>
                <h3 class="color-text-yellow mb-3">Important Notice For Tourism Industry</h5>
                    <p class="color-text-deepblue">
                        Please follow the instructions <a
                            href="{{ route('rebate-detail', ['slug' => 'rebates-for-tourism-industry']) }}">here</a>
                        before you apply for the rebate.
                    </p>
            </div>
        </div>
    </section>
    <!-- feature end -->
</main>
@endsection
