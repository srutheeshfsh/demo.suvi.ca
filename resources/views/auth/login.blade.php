@extends('layouts.app')

@section('body')

<body class="loginBg">
    <!--PRE LOADER-->

    <!--PRE LOADER-->
    <div class="wrapper">
        <!--HEADER-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-md-12 col-sm-12 col-lg-5">
                    <div class="logo"> <a href="index.php"> <img src="images/logo.png" alt="FSHDesign"
                                class="/img-fluid w-logo"> </a> </div>
                </div>
            </div>
            <div class="row loginBx">
                <div class="col-sm-12 col-12 col-md-5 col-lg-3">
                    <div class="loginTitle">LOGIN </div>

                    <form role="form" method="post" action="{{ route('login') }}" id="contact-form"
                        class="contact-form">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="email">EMAIL ADDRESS</label>

                                    <input id="email" type="email" placeholder="yourname@email.com"
                                        class="form-control @error('email') is-invalid @enderror" name="email"
                                        value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="password">PASSWORD</label>
                                    <input id="password" type="password" placeholder="*********"
                                        class="form-control @error('password') is-invalid @enderror" name="password"
                                        required autocomplete="current-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember"
                                            {{ old('remember') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="remember">
                                            {{ __('Remember Me') }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row pt-3">
                            <div class="col-md-12">
                                <input class="btn btn-submit" name="submit" type="submit">LOGIN</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--HEADER-->

    </div>
    <a href="#" id="back-to-top" title="Back to top"></a>
</body>
@endsection
