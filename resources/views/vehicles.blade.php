@extends('layouts.app')

@section('styles')
{{-- <link rel="stylesheet" type="text/css" href="/vendor/DataTables/datatables.min.css" /> --}}
<link rel="stylesheet" type="text/css"
    href="https://cdn.datatables.net/v/bs4/dt-1.10.24/sp-1.2.2/sl-1.3.3/datatables.min.css" />


@endsection

@section('content')
<main>
    <!--page title start -->
    <section class="page-title-area bg-overly slider-area slider-2" data-overlay="5"
        data-background="/img/bg/vehicle_bg.png">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">

                    <div class="page-title text-center" data-aos="fade-up" data-aos-delay="100">
                        <h1>Vehicles</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- page title end -->

    <!-- faq start -->
    <section class="faq-area pt-5 pb-100">
        <div class="container">
            <table class="datatable table table-striped thead-light table-bordered table-hover table-responsive">
                <thead>
                    <tr>
                        <th>Category</th>
                        <th>Make</th>
                        <th>Year</th>
                        <th>Model</th>
                        <th>Fuel Type</th>
                        <th>Battery Size</th>
                        <th>Vehicle Type</th>
                        <th>Base MSRP</th>
                        <th>Incentive (maximum)</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($vehicles as $vehicle)
                    <tr>
                        <td>{{ $vehicle->category }}</td>
                        <td>{{ $vehicle->make }}</td>
                        <td>{{ $vehicle->year }}</td>
                        <td>{{ $vehicle->model }}</td>
                        <td>{{ $vehicle->fuel_type }}</td>
                        <td>{{ $vehicle->battery_size }}</td>
                        <td>{{ $vehicle->vehicle_type }}</td>
                        <td>{{ $vehicle->base_msrp }}</td>
                        <td>{{ $vehicle->incentive }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </section>
    <!-- faq end -->
</main>
@endsection

@section('scripts')
{{-- <script type="text/javascript" src="/vendor/DataTables/datatables.min.js"></script> --}}
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.24/sp-1.2.2/sl-1.3.3/datatables.min.js">
</script>

<script>
    $(document).ready(function() {


        $('.datatable').DataTable({
            dom: 'Plfrtip',
            bLengthChange: false,
            language: {
                searchPanes: {
                    title: {
                        _: "Filters Selected - %d. Scroll down to see the results.",
                    }
                },
            },
            searchPanes: {
                dtOpts:{
                    select:{
                        style: 'multi'
                    }
                },
                orderable: false,
                cascadePanes: true,
                // clear: false,
                threshold: 1,
                columns: [1,2,4,7],
                layout: 'columns-4',
            },
            columnDefs: [
                {
                    searchPanes: {
                        show: true,
                    },
                    targets: [1]
                },
                {
                    searchPanes: {
                        show: true,
                    },
                    targets: [2]
                },
                {
                    searchPanes: {
                        show: true,
                    },
                    targets: [4]
                },
                {
                    type: 'num-fmt',
                    searchPanes: {
                        show: true,
                        options: [
                            {
                                label: 'Below 5000',
                                value: function(rowData, rowIdx) {
                                    return rowData[7] < 5000;
                                }
                            },
                            {
                                label: 'From 5000 to 10000',
                                value: function(rowData, rowIdx) {
                                    return rowData[7] >= 5000 && rowData[7] <= 10000;
                                }
                            },
                            {
                                label: 'From 10000 to 50000',
                                value: function(rowData, rowIdx) {
                                    return rowData[7] >= 10000 && rowData[7] <= 50000;
                                }
                            },
                            {
                                label: 'Over 50000',
                                value: function(rowData, rowIdx) {
                                    return rowData[7] > 50000;
                                }
                            },
                        ]
                    },
                    targets: [7]
                }
            ]
        });



         /* custom changes*/
        $('.dataTables_scrollBody').hide();
        $('body').on('focus','.dtsp-paneInputButton',function(){
           // $('.dataTables_scrollBody').hide();
            if($(this).parents('.dtsp-searchPane').find('.dataTables_wrapper .dataTables_scroll .dataTables_scrollBody').hasClass('showBox')){
                $(this).parents('.dtsp-searchPane').find('.dataTables_wrapper .dataTables_scroll .dataTables_scrollBody').slideUp( "hide" );
                $(this).parents('.dtsp-searchPane').find('.dataTables_wrapper .dataTables_scroll .dataTables_scrollBody').removeClass('showBox');
            }else{
                $(this).parents('.dtsp-searchPane').find('.dataTables_wrapper .dataTables_scroll .dataTables_scrollBody').addClass('showBox');
                $(this).parents('.dtsp-searchPane').find('.dataTables_wrapper .dataTables_scroll .dataTables_scrollBody').slideDown( "slow" );
            }

        });

        $('body').on('click','.dtsp-clearAll',function(){
             $('body').find('.showBox').slideUp("hide");
             $('body').find('.showBox').removeClass('showBox');
        });

        $('body').on('click',function(e){
            if ($(e.target).hasClass("dtsp-paneInputButton") || $(e.target).hasClass("dtsp-searchIcon")) {
                e.stopPropagation();
            } else {
                console.log($(this).attr('class'));
                $('body').find('.showBox').slideUp("hide");
                $('body').find('.showBox').removeClass('showBox');
            }
        });
    });
</script>
@endsection
