<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

use Barryvdh\DomPDF\Facade as PDF;
use TCG\Voyager\Facades\Voyager;

class ManufacturerApplication extends Notification
{
    use Queueable;

    protected $forms;
    protected $to_applicant;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($forms, $to_applicant)
    {
        $this->forms = $forms;
        $this->to_applicant = $to_applicant;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $forms = $this->forms;
        $form = $forms[0];
        $pdf = PDF::loadView('oem-form', compact('forms', 'form'));

        $subject = "SUVI - New OEM Application Received";
        $name = $form->representitive_name;
        $greeting = "Hello $name,";

        if (!$this->to_applicant) { // To SUVI
            $model_year = $form->model_year;
            $make = $form->vehicle_make;
            $model = $form->vehicle_model;
            $subject = "SUVI - OEM Application: $model_year $make $model";

            $greeting = "Hello,";
        }

        $mail = (new MailMessage)
            ->subject($subject)
            ->greeting($greeting)
            ->line('A new OEM application has been submited. The submission has been compiled and attached to this email as a pdf.')
            ->attachData($pdf->stream(), 'OEM application.pdf', [
                'mime' => 'application/pdf',
            ]);

        foreach ($forms as $form) {
            $mail = $mail->attach(Voyager::image($form->image));
        }

        return $mail;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
