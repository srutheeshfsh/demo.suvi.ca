@extends('layouts.app')

@section('styles')
<link href='/vendor/fullcalendar/lib/main.min.css' rel='stylesheet' />
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('content')
<main>
    <!-- page title start -->
    <section class="page-title-area bg-overly slider-area slider-2" data-overlay="5"
        data-background="/img/bg/success-stories-header.jpg">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">

                    <div class="page-title text-center" data-aos="fade-up" data-aos-delay="100">
                        <h1>Stories & Events</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- page title end -->

    <!-- causes start -->
    <div id="stories"></div>
    <section class="causes-area gray-bg pt-50 pb-100" >
        <div class="container">
            <div class="section-title text-center" data-aos="fade-up" data-aos-delay="100">
                <div class="bg-title">
                    <!-- <h1>Casuses</h1> -->
                </div>
                <!-- <h5>Good Causes</h5> -->
                <h2 class="color-text-greendouglous">Success Stories</h2>

            </div>
            <div class="carousel-wrap">
                <div class="owl-carousel row" data-autoplay="false">
                    @foreach ($stories as $story)
                    <div class="item">
                        <div class="col-lg-12 col-md-12">
                            <div class="causes-single mb-30" data-aos="fade-up" data-aos-delay="100">
                                <div class="causes-thumb">
                                    <a href="{{ route('success-story', ['slug' => $story->slug]) }}"><img
                                            src="{{ Voyager::image($story->cover_image) }}" alt=""></a>
                                </div>
                                <div class="causes-content">
                                    <div class="causes-head clearfix mb-10">
                                        <h4 class="color-text-greendouglous"><a
                                                href="{{ route('success-story', ['slug' => $story->slug]) }}">{{ $story->title }}</a>
                                        </h4>
                                        <!-- <span><i class="ti-location-pin"> </i> Bestory,Feni</span> -->
                                    </div>
                                    <div class="causes-text mb-25">
                                        <p>{{ $story->excerpt }}</p>
                                    </div>

                                    <div class="feature-link">
                                        <a class="Btn-border-yell"
                                            href="{{ route('success-story', ['slug' => $story->slug]) }}">Learn More
                                            <i class="ti-angle-double-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <div class="item">
                        <div class="col-lg-12 col-md-12">
                            <div class="causes-single mb-30" data-aos="fade-up" data-aos-delay="500">
                                <div class="causes-thumb">
                                    <img src="/img/causes/coming-soon.jpg" alt="">
                                </div>
                                <div class="causes-content">
                                    <div class="causes-head causes-head-3 clearfix mb-10">
                                        <h4 class="color-text-greendouglous"><a href="#">More Stories Coming
                                                Soon</a></h4>
                                        <!-- <span><i class="ti-location-pin"> </i> Bestory,Feni</span> -->
                                    </div>
                                    <div class="causes-text mb-25">
                                        <p>
                                            <br><br><br>
                                        </p>
                                    </div>
                                    <div class="feature-link">
                                        <a class="Btn-border-yell" href="#">Coming Soon <i
                                                class="ti-angle-double-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- causes end -->

    @if(setting('site.event_calendar_activation', false))
    <!-- counter start -->
    <div id="eventssection"></div>
    <section class="counter-area pt-120 pb-90" data-overlay="6" data-background="/img/bg/about-bg.jpg" >
        <div class="container">
            <h3 class="text-center text-white">Upcoming Events</h3>

            <div class="row mt-5">
                <div class="col-md-8 col-sm-12" id="calendar"></div>
                <div class="col-md-4 col-sm-12 card">
                    <div class="card-body">
                        <h5 class="card-title" id="date">{{ Carbon\Carbon::now()->format('F d') }}</h5>
                        <div class="list-group" id="events">
                            @foreach ($events as $event)
                            <a href="{{ $event->url }}"
                                class="list-group-item list-group-item-action flex-column align-items-start">
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1"> {{ $event->title }}
                                    </h5>
                                </div>
                                <p class="mb-1"> {!! $event->description !!}
                                </p>
                            </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- counter end -->
    @endif

</main>
@endsection

@section('scripts')
<script src='/vendor/fullcalendar/lib/main.js'></script>
<script src='/js/calendar.js'></script>
@endsection
