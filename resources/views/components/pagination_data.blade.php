@foreach ($vehicles as $vehicle)
<li class="list-group-item result">
    <div class="row">
        <div class="col-md-2 column-section my-auto">
            <img class="img-fluid rounded section-img" src="{{ $vehicle->image ? Voyager::image($vehicle->image) : '/img/mlogo.png' }}">
        </div>
        <div class="col-md-4 column-section my-auto align-items-sm-center">
            <h6>{{ $vehicle->make }}</h6>
            <h4>{{ $vehicle->model }} - {{ $vehicle->year }}</h4>
        </div>
        <div class="col-md-3 column-section column-section-seperate my-auto align-items-sm-center">
            @php
                $formatter = new NumberFormatter('en_CA',  NumberFormatter::CURRENCY);
            @endphp
            <p>
                Fuel Type:<span class="ml-1">{{ $vehicle->fuel_type }}</span> <br>
                Battery Size:<span class="ml-1"> {{ $vehicle->battery_size }}</span> <br>
                Type:<span class="ml-1">{{ $vehicle->vehicle_type }}</span> <br>
                MSRP:<span class="ml-1"> {{ $vehicle->base_msrp < 0 ? 'TBD' : str_replace('.00', '', $formatter->formatCurrency($vehicle->base_msrp, 'CAD')) . ' CAD' }}</span><br>
                {{-- Incentive:<span class="ml-1">{{ $vehicle->incentive < 0 ? 'TBD' : str_replace('.00', '', $formatter->formatCurrency($vehicle->incentive, 'CAD')) . ' CAD' }}</span> <br> --}}
            </p>
        </div>
		<div class="col-md-3 column-section column-section-seperate my-auto align-items-sm-center">

			<small>Estimated Rebate Amount</small>
			 <h3>{{ $vehicle->incentive < 0 ? 'TBD' : str_replace('.00', '', $formatter->formatCurrency($vehicle->incentive, 'CAD')) . ' CAD' }}</h3>

        </div>
    </div>
</li>
@endforeach
