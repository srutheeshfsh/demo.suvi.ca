$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

document.addEventListener('DOMContentLoaded', function () {
    var calendarEl = document.getElementById('calendar');
    var calendar = new FullCalendar.Calendar(calendarEl, {
        selectable: true,
        initialView: 'dayGridMonth',
        events: function (info, successCallback, failureCallback) {
            $.ajax({
                url: '/get-events',
                type: "POST",
                dataType: 'json',
                success: function (res) {
                    var events = [];
                    if (!!res) {
                        $.map(res, function (r) {
                            events.push({
                                id: r.id,
                                title: r.title,
                                start: r.start,
                                end: r.end,
                                description: r.description,
                                url: r.url,
                            });
                        });
                    }
                    successCallback(events);
                }
            });
        },
        dateClick: function (info) {
            $('#date').text(FullCalendar.formatDate(info.date, {
                month: 'long',
                day: 'numeric',
            }));

            date = info.date;

            $('#events').empty();
            events = calendar.getEvents().filter(function (event) {
                if (event.start.toDateString() == date.toDateString()) {
                    list = `
                    <a href="` + event.url +
                        `" class="list-group-item list-group-item-action flex-column align-items-start">
                        <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">` + nl2br(event.title) +
                        `</h5>
                        </div>
                        <p class="mb-1">` + nl2br(event.extendedProps.description) +
                        `</p>
                    </a>
                    `
                    $('#events').append(list);
                    return true;
                }
                return false;
            });
        },
    });
    calendar.render();
});

function nl2br(str, is_xhtml) {
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}
