@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
{{-- {{ config('app.name') }}
 --}}
<img src="{{ env('APP_URL') }}/img/logo/logo-tp.png" style="width: 30%" alt="SUVI">

@endcomponent
@endslot

{{-- Body --}}
{{ $slot }}

{{-- Subcopy --}}
@isset($subcopy)
@slot('subcopy')
@component('mail::subcopy')
{{ $subcopy }}
@endcomponent
@endslot
@endisset

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
{{-- © {{ date('Y') }} {{ config('app.name') }}. @lang('All rights reserved.') --}}
© 2021 {{ config('app.name') }}. @lang('All rights reserved.')
@endcomponent
@endslot
@endcomponent
