@extends('layouts.app')

@section('content')
<main>
    <!-- page title start -->
    <section class="page-title-area bg-overly slider-area slider-2" data-overlay="5"
        data-background="/img/bg/rebate-bg.jpg">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="page-title text-center" data-aos="fade-up" data-aos-delay="100">
                        <h1>Rebates</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- page title end -->

    <!-- about start -->
    <section class="about-area fix pt-50 pb-150">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-12 order-xl-1 order-md-2 order-1">
                    <div class="about-left">
                        <div class="section-title" data-aos="fade-up" data-aos-delay="100">
                            @php
                            $section = App\Section::where('name', 'Rebate page text')->first()
                            @endphp
                            @if ($section)
                            {!! $section->content !!}
                            @endif
                        </div>
                        <div class="about-btn" data-aos="fade-up" data-aos-delay="400">
                            <a href="/storage/{{ json_decode(setting('site.program_guide'))[0]->download_link }}"
                                download="{{ json_decode(setting('site.program_guide'))[0]->original_name }}"
                                class="thm-btn thm-btn-2">Program
                                Guide</a>

                            {{-- <p>Download the <a
                                    href="/storage/{{ json_decode(setting('site.program_guide'))[0]->download_link }}"
                            download="{{ json_decode(setting('site.program_guide'))[0]->original_name }}">Program
                            Guide</a></p> --}}
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-12 order-xl-2 order-md-1 order-2">
                    <div class="about-right-wrapper ">
                        <div class="about-right">
                            <div class="about-shape-bg">
                                <img src="/img/shape/about-shape-bg.png" alt="">
                            </div>
                            <div class="about-thumb-big" data-aos="fade-up" data-aos-delay="100">
                                <img src="/img/about/about-bg-03.jpg" alt="">
                            </div>
                            <div class="about-thumb-sml" data-aos="fade-left" data-aos-delay="300">
                                <img src="/img/about/about-smale-03.png" alt="">
                            </div>
                        </div>
                        <div class="about-right-text" data-aos="fade-right" data-aos-delay="300">
                            <img src="/img/about/about-smale-04.png" alt="">
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- about end -->

    @php
    $section = App\Section::where('name', 'Rebate lease options')->first()
    @endphp
    @if ($section)
    {!! $section->content !!}
    @endif

    @include('components.eligible-categories')

    @include('components.rebate-sliders')

</main>
@endsection
