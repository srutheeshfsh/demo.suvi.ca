<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithMapping;

class VehicleExport implements FromQuery, WithHeadings, WithMapping, ShouldAutoSize
{
    use Exportable;

    protected $vehicles;

    public function __construct($vehicles)
    {
        $this->vehicles = $vehicles;
    }

    public function query()
    {
        return $this->vehicles;
    }

    public function map($vehicles): array
    {
        return [
            $vehicles->category,
            $vehicles->vehicle_type,
            $vehicles->make,
            $vehicles->year,
            $vehicles->model,
            $vehicles->fuel_type,
            $vehicles->battery_size,
            $vehicles->base_msrp,
            $vehicles->incentive,
        ];
    }

    public function headings(): array
    {
        return [
            'Category',
            'Type',
            'Make',
            'Year',
            'Model',
            'Fuel Type',
            'Battery Size',
            'MSRP (CAD)',
            'Incentive (CAD)',
        ];
    }
}
