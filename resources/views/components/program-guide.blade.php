<p class="text-center">For more information, please download the Program Guide here.</p>
<section class="apply-today color-white  pb-50 text-center">
    <div class="container">
        <div class="about-btn aos-init aos-animate col-md-12" data-aos="fade-up" data-aos-delay="400">
            <a href="/storage/{{ json_decode(setting('site.program_guide'))[0]->download_link }}"
                class="thm-btn thm-btn-2"
                download="{{ json_decode(setting('site.program_guide'))[0]->original_name }}">Download
                the Program
                Guide</a>
        </div>
    </div>
</section>
