<?php

namespace App\Helpers;

class Image
{
    /**
     * Store uploaded images in storage.
     */
    public static function storeImage($file, $filename)
    {
        $des_folder = storage_path('app/public/uploaded_images/');
        // Create the folder if not exist
        if (!file_exists($des_folder)) {
            mkdir($des_folder, 0777, true);
        }
        $destination = $des_folder;
        // IF OS is Windows
        if (env('ON_WINDOWS', false)) {
            $destination = str_replace('/', '\\', $destination);
        }

        $file->move($destination, $filename);

        return '/uploaded_images/' . $filename;
    }
}
