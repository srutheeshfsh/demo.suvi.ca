@extends('layouts.app')

@section('content')
<main>
    <!-- page title start -->
    <section class="page-title-area bg-overly slider-area slider-2" data-overlay="5"
        data-background="/img/bg/faq-bg.png">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">

                    <div class="page-title text-center" data-aos="fade-up" data-aos-delay="100">
                        <h1>FAQs</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- page title end -->

    <!-- faq start -->
    <section class="faq-area pt-50 pb-100">
        <div class="container">
            <div class="row">
                @foreach ($faqs->chunk($faqs->count()/2 + 1) as $chunk)
                <div class="col-xl-6 col-lg-6">
                    <div class="faq-left mb-20" data-aos="fade-up" data-aos-delay="100">
                        <div class="faq-wrapper">
                            <div class="accordion" id="accordion{{ $loop->iteration }}">
                                @foreach ($chunk as $faq)
                                <div class="card">
                                    <div class="card-header" id="heading-{{ $loop->parent->iteration }}-{{ $loop->iteration }}">
                                        <h5 class="mb-0">
                                            <a href="#" class="btn-link" data-toggle="collapse"
                                                data-target="#collapse-{{ $loop->parent->iteration }}-{{ $loop->iteration }}" aria-expanded="true"
                                                aria-controls="collapse-{{ $loop->parent->iteration }}-{{ $loop->iteration }}">
                                                {{ $faq->question }}
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse-{{ $loop->parent->iteration }}-{{ $loop->iteration }}" class="collapse"
                                        aria-labelledby="heading-{{ $loop->parent->iteration }}-{{ $loop->iteration }}"
                                        data-parent="#accordion{{ $loop->parent->iteration }}">
                                        <div class="card-body">
                                            {!! $faq->answer !!}
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
</main>
@endsection
