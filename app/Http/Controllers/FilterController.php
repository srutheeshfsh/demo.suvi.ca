<?php

namespace App\Http\Controllers;

use App\EligibleCategory;
use App\Event;
use App\Exports\VehicleExport;
use App\Helpers\Image;
use App\Imports\VehicleImport;
use App\ManufacturerApplication;
use App\Notifications\ManufacturerApplication as NotificationsManufacturerApplication;
use App\Template;
use App\TemplateCategory;
use App\Vehicle;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class FilterController extends Controller
{
    function index(Request $request)
    {
        $queries = [];

        $category = $request['category'];
        $make = $request['make'];
        $year = $request['year'];
        $fuel_type = $request['fuel_type'];
        $vehicle_type = $request['vehicle_type'];
        $base_msrp = urldecode($request['base_msrp']);
        $match = $request['match'];

        // Must have category
        if ($category && in_array($category, Vehicle::categories())) {
            $vehicles = Vehicle::where('category', $category);
            // Append the input queries in response
            foreach ($queries as $key => $value) {
                $vehicles->appends($key, $value);
            }
            $queries['category'] = $category;

            $filters = [
                "make" => $vehicles->get()->pluck('make')->unique(),
                "year" => $vehicles->get()->pluck('year')->unique(),
                "fuel_type" => $vehicles->get()->pluck('fuel_type')->unique(),
                "vehicle_type" => $vehicles->get()->pluck('vehicle_type')->unique(),
            ];
            $min_base_msrp = $vehicles->where('base_msrp', '>', 0)->min('base_msrp') - 1;
            $max_base_msrp = $vehicles->where('base_msrp', '>', 0)->max('base_msrp') + 1;

            $category = EligibleCategory::where('name', $category)->firstOrFail();
        } else {
            abort(404);
        }

        if ($make) {
            $vehicles = $vehicles->where(function ($q) use ($make) {
                foreach ($make as $m) {
                    $q->orWhere('make', $m);
                }
            });
            $queries['make'] = $make;
        }

        if ($year) {
            $vehicles = $vehicles->where(function ($q) use ($year) {
                foreach ($year as $y) {
                    $q->orWhere('year', $y);
                }
            });
            $queries['year'] = $year;
        }

        if ($fuel_type) {
            $vehicles = $vehicles->where(function ($q) use ($fuel_type) {
                foreach ($fuel_type as $f) {
                    $q->orWhere('fuel_type', $f);
                }
            });
            $queries['fuel_type'] = $fuel_type;
        }
        if ($vehicle_type) {
            $vehicles = $vehicles->where(function ($q) use ($vehicle_type) {
                foreach ($vehicle_type as $f) {
                    // $q->orWhere('vehicle_type', $f);
                    $q->orWhere('vehicle_type', 'LIKE', '%' . $f . '%');
                }
            });
            $queries['vehicle_type'] = $vehicle_type;
        }

        if ($base_msrp) {
            $range = explode(' - ', str_replace('$', '', $base_msrp));

            $vehicles = $vehicles->where(function($q) use($range) {
                $q->where('base_msrp', '>=', $range[0])
                ->where('base_msrp', '<=', $range[1])
                ->orWhere('base_msrp', '<', 0); // Let TBD entry bypass this filter
            });
            $queries['base_msrp'] = $base_msrp;
        }

        if ($match) {
            // $words = explode(' ', $match);
            $words = explode(',', $match);
            $columns = [
                'make',
                'year',
                'model',
                'fuel_type',
                'battery_size',
                'incentive',
                'base_msrp',
                'vehicle_type'
            ];

            $vehicles->where(function ($q) use ($columns, $words) {
                foreach ($columns as $column) {
                    $q->orWhere(function ($q) use ($column, $words) {
                        foreach ($words as $word) {
                            $q->orwhere($column, 'LIKE', '%' . $word . '%');
                        }
                    });
                }
            });
           

            $queries['match'] = $match;
        }

        if (!isset($range)) {
            $range = [$min_base_msrp, $max_base_msrp];
        }

        if ($request->route()->getName() == 'vehicle.export') {
            return (new VehicleExport($vehicles))->download('vehicles.xlsx', \Maatwebsite\Excel\Excel::XLSX);
        }

        $vehicles = $vehicles->paginate(15);

        if ($request->ajax()) {
            return view('components.pagination_data', compact('vehicles', 'queries', 'min_base_msrp', 'max_base_msrp', 'range', 'filters', 'category'))->render();
        } else {
            return view('eligible-vehicles', compact('vehicles', 'queries', 'min_base_msrp', 'max_base_msrp', 'range', 'filters', 'category'));
        }
    }
}
