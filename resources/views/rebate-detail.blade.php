@extends('layouts.app')

@section('content')
<main>
    <!--page title start -->
    <section class="page-title-area bg-overly slider-area slider-2" data-overlay="5"
        data-background="{{ Voyager::image($page->banner_image) }}">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">

                    <div class="page-title text-center" data-aos="fade-up" data-aos-delay="100">
                        <h1>{{ $page->title }}</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- page title end -->

    <!-- faq start -->
    <section class="faq-area page pt-50">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 offset-lg-1 offset-xl-1 col-md-12 col-xl-10 pt-5" data-aos="fade-up"
                    data-aos-delay="100">
                    {!! $page->content !!}
                    <br>

                    @include('components.program-guide')
                </div>
            </div>
        </div>
        @if (!Str::contains($page->slug, 'tourism'))
        @include('components.eligible-categories')
        @endif
    </section>
</main>
@endsection

@section('scripts')
@if (Str::contains($page->slug, 'tourism'))
<script>
    $('#tourism_form').attr('href', '/storage/{{ json_decode(setting('site.tourism_form'))[0]->download_link }}');
    $('#tourism_form').attr('download', '{{ json_decode(setting('site.tourism_form'))[0]->original_name }}');
</script>
@endif
@endsection
